import 'package:com/models/account/banner.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/stream_object.dart';

class Dashboard {
  int bannerDelay;
  List<StreamObject> streams;
  List<Banner> banners;
  // List<StreamObject> streams = [
  //   StreamObject(
  //       icon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //       selectedIcon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //       name: "Item 1",
  //       courses: [
  //         Course(
  //             title: "Course 101",
  //             duration: "1 year",
  //             amount: "12,000",
  //             type: CourseTypes("PG")),
  //         Course(
  //             title: "Course 102",
  //             duration: "2 years",
  //             amount: "24,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 103",
  //             duration: "3 years",
  //             amount: "43,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 104",
  //             duration: "4 years",
  //             amount: "66,000",
  //             type: CourseTypes("PG")),
  //       ]),
  //   StreamObject(
  //       icon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //       selectedIcon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //       name: "Item 2",
  //       courses: [
  //         Course(
  //             title: "Course 101",
  //             duration: "1 year",
  //             amount: "12,000",
  //             type: CourseTypes("PG")),
  //         Course(
  //             title: "Course 102",
  //             duration: "2 years",
  //             amount: "24,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 103",
  //             duration: "3 years",
  //             amount: "43,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 104",
  //             duration: "4 years",
  //             amount: "66,000",
  //             type: CourseTypes("PG")),
  //       ]),
  //   StreamObject(
  //       icon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //       selectedIcon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //       name: "Item 3",
  //       courses: [
  //         Course(
  //             title: "Course 101",
  //             duration: "1 year",
  //             amount: "12,000",
  //             type: CourseTypes("PG")),
  //         Course(
  //             title: "Course 102",
  //             duration: "2 years",
  //             amount: "24,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 103",
  //             duration: "3 years",
  //             amount: "43,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 104",
  //             duration: "4 years",
  //             amount: "66,000",
  //             type: CourseTypes("PG")),
  //       ]),
  //   StreamObject(
  //       icon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //       selectedIcon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //       name: "Item 4",
  //       courses: [
  //         Course(
  //             title: "Course 101",
  //             duration: "1 year",
  //             amount: "12,000",
  //             type: CourseTypes("PG")),
  //         Course(
  //             title: "Course 102",
  //             duration: "2 years",
  //             amount: "24,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 103",
  //             duration: "3 years",
  //             amount: "43,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 104",
  //             duration: "4 years",
  //             amount: "66,000",
  //             type: CourseTypes("PG")),
  //       ]),
  //   StreamObject(
  //       icon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //       selectedIcon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //       name: "Item 5",
  //       courses: [
  //         Course(
  //             title: "Course 101",
  //             duration: "1 year",
  //             amount: "12,000",
  //             type: CourseTypes("PG")),
  //         Course(
  //             title: "Course 102",
  //             duration: "2 years",
  //             amount: "24,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 103",
  //             duration: "3 years",
  //             amount: "43,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 104",
  //             duration: "4 years",
  //             amount: "66,000",
  //             type: CourseTypes("PG")),
  //       ]),
  //   StreamObject(
  //       icon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //       selectedIcon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //       name: "Item 6",
  //       courses: [
  //         Course(
  //             title: "Course 101",
  //             duration: "1 year",
  //             amount: "12,000",
  //             type: CourseTypes("PG")),
  //         Course(
  //             title: "Course 102",
  //             duration: "2 years",
  //             amount: "24,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 103",
  //             duration: "3 years",
  //             amount: "43,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 104",
  //             duration: "4 years",
  //             amount: "66,000",
  //             type: CourseTypes("PG")),
  //       ]),
  //   StreamObject(
  //       icon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //       selectedIcon:
  //           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //       name: "Item 7",
  //       courses: [
  //         Course(
  //             title: "Course 101",
  //             duration: "1 year",
  //             amount: "12,000",
  //             type: CourseTypes("PG")),
  //         Course(
  //             title: "Course 102",
  //             duration: "2 years",
  //             amount: "24,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 103",
  //             duration: "3 years",
  //             amount: "43,000",
  //             type: CourseTypes("UG")),
  //         Course(
  //             title: "Course 104",
  //             duration: "4 years",
  //             amount: "66,000",
  //             type: CourseTypes("PG")),
  //       ]),
  // ];
  List<City> topCities;
//   List<City> topCities = [
//     City(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//         icon:
//             "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//         name: "City 1",
//         id: 1),
//     City(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//         icon:
//             "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//         name: "City 2",
//         id: 2),
//     City(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//         icon:
//             "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//         name: "City 3",
//         id: 3),
//     City(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//         icon:
//             "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//         name: "City 4",
//         id: 4),
//     City(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//         icon:
//             "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//         name: "City 5",
//         id: 5)
//   ];
  List<College> topColleges;
  List<College> bookmarkedColleges;

//   List<College> topColleges = [
//     College(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//       image: "https://miro.medium.com/max/1400/1*fvE415nzWc5pOUy1lQoGZA.png",
//       logo:
//           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//       name: "College 1",
//     ),
//     College(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//       image: "https://miro.medium.com/max/1400/1*tmJvATp4uFZk2ehlX1exAQ.png",
//       logo:
//           "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//       name: "College 2",
//     ),
//     College(
// //      selectedIcon: "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
//         image: "https://miro.medium.com/max/2414/0*llO5f0NhDCAp5Y7j.png",
//         logo:
//             "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
//         name: "College 3",
//         bookmarked: true),
//   ];

  Dashboard({this.streams, this.topCities, this.topColleges, this.bookmarkedColleges, this.banners, this.bannerDelay});

  static Dashboard _dashboard;

  static Dashboard fromJson(dynamic json) {
    _dashboard = Dashboard(
      streams: StreamObject.fromJsonList(json['streams']),
      topCities: City.fromJsonList(json['topCities']),
      topColleges: College.fromJsonList(json['topColleges']),
      bookmarkedColleges: College.fromJsonList(json['bookmarkedColleges'] ?? []),
      banners: Banner.fromJsonList(json['banners'] ?? []),
      bannerDelay: json['banner_delay'] ?? 15
    );
    return _dashboard;
  }

  dynamic toJson() {
    return {};
  }

  bool get isEmpty {
    return false;
  }

  void save(){
    Dashboard._dashboard = this;
  }

  static Dashboard persistence(){
    return _dashboard;
  }
}
