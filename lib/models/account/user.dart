import 'package:com/models/base_model.dart';
import 'package:com/services/urls.dart';
import 'package:com/utils/utils.dart';

class User extends BaseModel{
  String name;
  String phone;
  String avatar;
  String token;
  String group;
  bool hasStreams = false;
  bool isPartner = false;
  bool phoneVerified = false;


  User({this.name, this.phone, this.token, this.avatar, this.group, this.isPartner, this.phoneVerified});

  static User fromJson(dynamic js){
    String avatar = js['avatar'];
    if(isNotEmpty(avatar)){
      avatar = !avatar.startsWith("http") ? "${Urls.mediaBase}/$avatar" : avatar;
    }
    return User(
      name: js['name'],
      phone: js['phone'],
      token: js['api_token'],
      group: js['group'],
      avatar: avatar,
      isPartner: js['is_partner'] == 1,
      phoneVerified: js['phone_verified'] == 1,
    );
  }

  dynamic toJson(){
    return {
      "name": name,
      "phone": phone,
      "api_token": token,
      'group': group,
      'avatar': avatar,
      'is_partner': isPartner ? 1 : 0,
      "phone_verified": phoneVerified ? 1 : 0,
    };
  }

  static User _user;

  static User get(){
    return _user;
  }

  void save(){
    User._user = this;
  }

}