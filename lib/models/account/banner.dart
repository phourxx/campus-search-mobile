import 'package:com/services/urls.dart';

class Banner{
  String image;
  String url;

  Banner({this.image, this.url});

  static Banner fromJson(dynamic js){
    return Banner(image: "${Urls.mediaBase}/${js['image']}", url: js['url']);
  }

  static List<Banner> fromJsonList(List<dynamic> jsonList){
    return jsonList.map((e) => fromJson(e)).toList();
  }
}