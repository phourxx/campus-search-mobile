import 'package:com/models/base_model.dart';
import 'package:com/services/urls.dart';

import 'college.dart';

class StreamObject extends BaseModel {
  int id;
  String icon;
  String selectedIcon;
  String name;
  List<Course> courses;
  bool selected = false;

  StreamObject({this.id, this.icon, this.selectedIcon, this.name, this.courses});

  static StreamObject fromJson(dynamic json){
    return StreamObject(
      id: json["id"],
      icon: "${Urls.mediaBase}/${json['icon']}",
      selectedIcon: "${Urls.mediaBase}/${json['selected_icon']}",
      name: json["name"],
      courses: Course.fromJsonList(json["courses"] ?? []),
    );
  }

  static List<StreamObject> fromJsonList(List<dynamic> jsonList){
    return jsonList.map((js){
      return StreamObject.fromJson(js);
    }).toList();
  }

  dynamic toJson(){
    return {};
  }

}