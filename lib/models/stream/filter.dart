class Filter {
  List<int> coursesId;
  int cityId;

  Filter({List<int> coursesId, this.cityId}) {
    this.coursesId = coursesId ?? [];
  }

  Filter.clone(Filter filter)
      : this(coursesId: filter.coursesId, cityId: filter.cityId);

  @override
  bool operator ==(other) {
    return cityId == other.cityId &&
        coursesId.every((id) => other.coursesId.contains(id));
  }
}
