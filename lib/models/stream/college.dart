import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/services/urls.dart';
import 'package:com/utils/enum.dart';
import 'package:com/utils/utils.dart';

class College {
  int id;
  String logo;
  String image;
  String name;
  String about;
  String address;
  String phone;
  String email;
  String website;
  num longitude;
  num latitude;
  City city;
  bool bookmarked;
  List<Course> courses = [];
  bool isPremium;
  // List<Course> courses = [
  //   Course(
  //       title: "Course 101", duration: "1 year", amount: "12,000", type: CourseTypes("PG")),
  //   Course(
  //       title: "Course 102", duration: "2 years", amount: "24,000", type: CourseTypes("UG")),
  //   Course(
  //       title: "Course 103", duration: "3 years", amount: "43,000", type: CourseTypes("UG")),
  //   Course(
  //       title: "Course 104", duration: "4 years", amount: "66,000", type: CourseTypes("PG")),
  // ];
  List<CollegeImage> images;
  // List<CollegeImage> images = [
  //   CollegeImage(
  //       caption: "Library",
  //       imageUrl: "https://miro.medium.com/max/2414/0*llO5f0NhDCAp5Y7j.png"),
  //   CollegeImage(
  //       caption: "Library 2",
  //       imageUrl: "https://miro.medium.com/max/2414/0*llO5f0NhDCAp5Y7j.png"),
  //   CollegeImage(
  //       caption: "Library 3",
  //       imageUrl: "https://miro.medium.com/max/2414/0*llO5f0NhDCAp5Y7j.png"),
  //   CollegeImage(
  //       caption: "Library 4",
  //       imageUrl: "https://miro.medium.com/max/2414/0*llO5f0NhDCAp5Y7j.png"),
  //   CollegeImage(
  //       caption: "Library 5",
  //       imageUrl: "https://miro.medium.com/max/2414/0*llO5f0NhDCAp5Y7j.png"),
  // ];
  List<News> news;
  // List<News> news = [
  //   News(
  //       content:
  //           "Last date for submitting application to MBA Admissions 2020, for both (Full Time) and (Part Time), has been extended till May 15, 2020 April 15, 2020",
  //       dateTime: DateTime.now().subtract(Duration(minutes: 10))),
  //   News(
  //       content:
  //           "Last date for submitting application to MBA Admissions 2020, for both (Full Time) and (Part Time), has been extended till May 15, 2020 April 15, 2020",
  //       dateTime: DateTime.now().subtract(Duration(minutes: 20))),
  // ];
  List<StreamObject> streams;
  // List<StreamObject> streams = [
  //   StreamObject(
  //     icon:
  //         "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //     selectedIcon:
  //         "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //     name: "Item 1",
  //   ),
  //   StreamObject(
  //     icon:
  //         "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //     selectedIcon:
  //         "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //     name: "Item 2",
  //   ),
  //   StreamObject(
  //     icon:
  //         "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
  //     selectedIcon:
  //         "https://ph7cms.com/wp-content/uploads/2019/02/long-path-dating-startup.png",
  //     name: "Item 3",
  //   ),
  // ];

  College({this.id, this.logo, this.image, this.name, this.bookmarked, this.about, this.phone, this.address, this.email, this.website, this.longitude, this.latitude});

  static College fromJson(dynamic json) {
    College college = College(
      id: json['id'],
      logo: "${Urls.mediaBase}/${json['logo']}",
      image: "${Urls.mediaBase}/${json['image']}",
      name: json['name'],
      bookmarked: (json['bookmarked'] ?? 0) == 1,
      about: json['about'],
      phone: json['phone'],
      address: json['address'],
      email: json['email'],
      website: json['website'],
      longitude: json['longitude'],
      latitude: json['latitude']
    );
    college.news = News.fromJsonList(json['news'] ?? []);
    college.courses = Course.fromJsonList(json['coursefee'] ?? []);
    college.images = CollegeImage.fromJsonList(json['gallery'] ?? []);
    college.streams = StreamObject.fromJsonList(json['streams'] ?? []);
    college.city = City.fromJson(json['city'] ?? {});
    print(json['city']);
    college.isPremium = json['college_type'] == 'premium';
    return college;
  }

  static List<College> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((js){
      return College.fromJson(js);
    }).toList();
  }

  dynamic toJson() {
    return {
      "name": name,
      'about': about,
      'email': email,
      'phone': phone,
      'address': address,
      'website': website,
      'longitude': longitude,
      'latitude': latitude,
      'image': image,
      'city_id': city.id,
      'logo': logo,
      'courses': courses.map((e) => e.toJson()).toList(),
    };
  }

  bool validForPost() {
    return false;
  }
}

class Course {
  int id;
  int courseId;
  String title;
  String duration;
  String amount;
  String partnerAmount;
  CourseTypes type;
  bool selected = false;

  Course({this.id, this.courseId, this.title, this.duration, this.amount, this.partnerAmount, this.type});

  static Course fromJson(dynamic json) {
    return Course(
      id: json['id'],
      courseId: json['course_id'],
      title: json['title'],
      duration: json['duration'],
      amount: json['amount'],
      partnerAmount: json['partner_amount'] ?? "0",
      type: CourseTypes(json['type'])
    );
  }

  static List<Course> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((js){
      return Course.fromJson(js);
    }).toList();
  }

  String _key;

  String get key{
    if(isEmpty(_key)){
      _key = randomString(10);
    }
    return _key;
  }

  dynamic toJson() {
    return {
      "course_id": id,
      "amount": amount,
      "duration": duration,
      "type": type.value,
    };
  }
}

class News {
  String content;
  DateTime dateTime;

  News({this.content, this.dateTime});

  static News fromJson(dynamic json) {
    return News(content: json['text'], dateTime: DateTime.parse(json['created_at']));
  }

  static List<News> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((js){
      return News.fromJson(js);
    }).toList();
  }
}

class CollegeImage {
  String caption;
  String imageUrl;

  CollegeImage({this.caption, this.imageUrl});

  static CollegeImage fromJson(dynamic json) {
    return CollegeImage(caption: json['caption'], imageUrl: "${Urls.mediaBase}/${json['image']}");
  }

  static List<CollegeImage> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((js){
      return CollegeImage.fromJson(js);
    }).toList();
  }
}
class CourseTypes extends Enum{
  static const phd = CourseTypes.internal("Ph.D", 'PHD');
  static const ug = CourseTypes.internal("Undergraduate", 'UG');
  static const pg = CourseTypes.internal('Postgraduate', 'PG');


  static const List<CourseTypes> values = [
    ug,
    pg,
    phd,
  ];


  const CourseTypes.internal(String text, String value) : super.internal(text, value);
  factory CourseTypes(String raw) =>
      values.singleWhere((val) => val.value == raw, orElse: () => values[0]);
}