import 'package:com/services/urls.dart';

class City {
  int id;
  int collegeCount;
  String icon;
  String name;

  City({this.id, this.icon, this.name, this.collegeCount});

  static City fromJson(dynamic json) {
    return City(id: json['id'], icon: "${Urls.mediaBase}/${json['icon']}", name: json['name'], collegeCount: json['college_count'] ?? 0);
  }

  static List<City> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((js){
      return City.fromJson(js);
    }).toList();
  }

  dynamic toJson() {
    return {};
  }
}
