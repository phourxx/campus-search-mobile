
import 'package:com/components/app_scaffold.dart';
import 'package:com/controllers/dashboard_controller.dart';
import 'package:com/models/account/user.dart';
import 'package:com/utils/constants.dart';
import 'package:com/views/dashboard/tabs/explore_colleges_tab.dart';
import 'package:com/views/dashboard/tabs/home_tab.dart';
import 'package:com/views/dashboard/tabs/profile_tab.dart';
import 'package:com/views/dashboard/tabs/trending_colleges_tab.dart';
import 'package:com/views/onboarding/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _DashboardScreenState();

}

class _DashboardScreenState extends State<DashboardScreen> with TickerProviderStateMixin implements DashboardView {

  DashboardController controller;
  User user = User.get();
  int selectedTabIndex = 0;
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(
      initialIndex: 0,
      length: 4,
      vsync: this
    );
    tabController.addListener((){
      if(selectedTabIndex != tabController.index){
        setState(() {
          selectedTabIndex = tabController.index;
        });
      }
    });
    controller = DashboardController(this);
    controller.load();
    super.initState();
  }

  Widget _buildBody(_){
    return TabBarView(
      controller: tabController,
      children: [
        HomeTab(this, controller),
        ExploreCollegesTab(),
        TrendingCollegesTab(),
        ProfileTab()
      ]
    );
  }

  Widget _buildBottomNav(){
    return BottomNavigationBar(
      currentIndex: selectedTabIndex,
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      selectedFontSize: 14,
      unselectedFontSize: 14,
      items: [
        BottomNavigationBarItem(
          icon: Image.asset("assets/images/home_icon_inactive.png"),
          activeIcon: Image.asset("assets/images/home_icon_active.png"),
          title: SizedBox.shrink()
        ),
        BottomNavigationBarItem(
          icon: Image.asset("assets/images/search_icon_inactive.png"),
          activeIcon: Image.asset("assets/images/search_icon_active.png"),
          title: SizedBox.shrink()
        ),
        BottomNavigationBarItem(
          icon: Image.asset("assets/images/colleges_icon_inactive.png"),
          activeIcon: Image.asset("assets/images/colleges_icon_active.png"),
          title: SizedBox.shrink()
        ),
        BottomNavigationBarItem(
          icon: Image.asset("assets/images/profile_icon_inactive.png"),
          activeIcon: Image.asset("assets/images/profile_icon_active.png"),
          title: SizedBox.shrink()
        )
      ],
      onTap: (int tab) {
        setState(() {
//          controller.selectedTab = controller.tabItems[tab];
          tabController.animateTo(tab);
          selectedTabIndex = tab;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      bottomNavigationBar: _buildBottomNav(),
      builder: _buildBody,
    );
  }

  @override
  void gotoTab(int index) {
    tabController.animateTo(index);
  }

  @override
  void onUnauthenticated() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(USER_DATA_PREF);
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
        builder: (c)=> LoginScreen()
    ), (r)=> r == null);
  }

  @override
  void refresh() {
    // TODO: implement refresh
  }

}

abstract class DashboardView{
  void refresh();
  void gotoTab(int index);
  void onUnauthenticated();
}