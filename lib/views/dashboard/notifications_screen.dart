import 'package:com/components/app_scaffold.dart';
import 'package:com/controllers/dashboard_controller.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/components/image_progress.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class NotificationsScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _NotificationsScreenState();

}

class _NotificationsScreenState extends State<NotificationsScreen> {
  DashboardController controller;


  @override
  void initState() {
    controller = DashboardController(null);
    controller.fetchNotifications();
    super.initState();
  }

  Widget _buildNotifications(_){
    return SafeArea(
        left: false,
        right: false,
        bottom: false,
        child: Container(
            color: Colors.white,
            padding: EdgeInsets.only(bottom: 15),
            child: ListView.separated(
              itemCount: 5,
              itemBuilder: (c, i){
                return InkWell(
                  onTap: (){

                  },
                  child: Container(
                      color: i==0 ? Color(0xFFE5F0FB) : null,
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Column(
                          children: <Widget>[
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(child: Text(
                                    "Lorem ipsum dolor sit amet",
                                    style: Theme.of(context).textTheme.title.copyWith(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 19
                                    ),
                                  )),
                                  SizedBox(width: 15),
                                  Text(
                                    "1hr ago",
                                    style: TextStyle(
                                        fontSize: 9
                                    ),
                                  ),
                                ]
                            ),
                            SizedBox(height: 10),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(child: Text(
                                    "Lorem ipsum dolor sit amet, consectetur.",
                                    style: TextStyle(
                                        fontSize: 13
                                    ),
                                  )),
                                  SizedBox(width: 15),
                                  Image.network(
                                    "https://ph7cms.com/wp-content/uploads/2019/02/imagination-dating-business-idea.png",
                                    fit: BoxFit.contain,
                                    width: 25,
                                    height: 25,
                                    loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                                      if (loadingProgress == null) return child;
                                      return Center(
                                        child: SizedBox(
                                            width: 25,
                                            height: 25,
                                            child: ImageProgress(loadingProgress)
                                        ),
                                      );
                                    },
                                  ),
                                ]
                            ),
                          ]
                      )
                  )
                );
              },
              separatorBuilder: (c, i)=> Divider(color: Color(0xFFE2E2E2), height: 1),
            )
        )
    );
  }

  Widget _buildLoading(_){
    return SafeArea(
      left: false,
      right: false,
      bottom: false,
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 15),
        child: Column(
          children: <Widget>[
            Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: 70
              ),
            ),
            SizedBox(height: 20),
            Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: 70
              ),
            ),
            SizedBox(height: 20),
            Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: 70
              ),
            ),
            SizedBox(height: 20),
            Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: 70
              ),
            ),
            SizedBox(height: 20),
            Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: 70
              ),
            ),
            SizedBox(height: 20),
            Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: 70
              ),
            )
          ],
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PageState>(
      initialData: PageState.loading,
      stream: controller.pageStateStream,
      builder: (c, snapshot){
        return AppScaffold(
          showAppBar: true,
          title: "Notifications",
          centerTitle: true,
          state: AppState(
            pageState: PageState.loaded
          ),
          loadingBuilder: _buildLoading,
          builder: _buildNotifications
        );
      }
    );
  }

}