import 'package:com/components/rounded_button.dart';
import 'package:com/components/stream_grid_item.dart';
import 'package:com/controllers/dashboard_controller.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/city/list_city_screen.dart';
import 'package:com/views/college/stream_colleges_screen.dart';
import 'package:com/views/stream/select_stream_course.dart';
import 'package:com/views/stream/select_stream_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class HomeStreamsSection extends StatelessWidget {

  DashboardController controller;

  HomeStreamsSection(this.controller);

  Widget _buildBody(context){
    if(controller.dashboard.streams.isEmpty){
      return Container(
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            SizedBox(height: 15),
            Text("No streams added yet", style: TextStyle(color: Theme.of(context).primaryColor)),
            SizedBox(height: 15),
            SizedBox(
              width: 160,
              child: RoundedButton(
                label: "Add Streams",
                labelSize: 14,
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (c)=> SelectStreamScreen()
                    )
                  );
                }
              )
            )
          ],
        ),
      );
    }
    return Wrap(
      direction: Axis.horizontal,
      children: controller.dashboard.streams.map((stream){
        return Container(
          margin: EdgeInsets.only(right: 10, top: 10),
          width: (MediaQuery.of(context).size.width/3) - (2*10),
          child: StreamGridItem(
            stream: stream,
            width: (MediaQuery.of(context).size.width/3) - (2*10),
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (c)=> SelectStreamCourseScreen(stream: stream)
                  )
              );
            },
          ),
        );
      }).toList(),
    );
    // return SizedBox(
    //   height: kStreamGridItemMinHeight,
    //   child: ListView.builder(
    //     itemCount: controller.dashboard.streams.length,
    //     scrollDirection: Axis.horizontal,
    //     itemBuilder: (c, index){
    //       return Container(
    //         margin: EdgeInsets.only(right: 10),
    //         child: StreamGridItem(
    //           stream: controller.dashboard.streams[index],
    //           onTap: (){
    //             Navigator.push(
    //                 context,
    //                 MaterialPageRoute(
    //                     builder: (c)=> StreamCollegesScreen(controller.dashboard.streams[index])
    //                 )
    //             );
    //           },
    //         ),
    //       );
    //     }
    //   )
    // );
  }

  Widget _buildLoadingBody(){
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(child: Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: kStreamGridItemMinHeight
              ),
            )),
            SizedBox(width: 10),
            Expanded(child: Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: kStreamGridItemMinHeight
              ),
            )),
            SizedBox(width: 10),
            Expanded(child: Shimmer.fromColors(
              baseColor: Colors.grey[300].withOpacity(1),
              highlightColor: Colors.grey[100].withOpacity(1),
              child: Container(
                  color: Colors.black,
                  height: kStreamGridItemMinHeight
              ),
            )),
          ]
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("Your Streams", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            (controller.pageState == PageState.loaded && controller.dashboard.streams.isNotEmpty) ?
              FlatButton(
                padding: EdgeInsets.zero,
                onPressed: controller.pageState == PageState.loading ? null : (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (c)=> SelectStreamScreen()
                      )
                  );
                },
                child: Text("Add more")
              ) : SizedBox.shrink()
          ]
        ),
        controller.pageState == PageState.loading ?
        _buildLoadingBody() : _buildBody(context)
      ],
    );
  }

}
