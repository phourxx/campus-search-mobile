import 'package:com/components/city_grid_item.dart';
import 'package:com/controllers/dashboard_controller.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/city/list_city_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class HomeCitiesSection extends StatelessWidget {

  DashboardController controller;

  HomeCitiesSection(this.controller);

  Widget _buildBody(context){
    int len = controller.dashboard.topCities.length > 6
        ? 6 : controller.dashboard.topCities.length;
    if(len == 0){
      return Container(
        alignment: Alignment.center,
        child: Text("No cities yet", style: TextStyle(color: Theme.of(context).primaryColor)),
      );
    }
    return GridView.builder(
      itemCount: len,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: kCityGridItemVerticalSpacing,
        mainAxisSpacing: kCityGridItemVerticalSpacing
      ),
      itemBuilder: (c, index){
        return CityGridItem(
          city: controller.dashboard.topCities[index],
          onTap: (){},
        );
      }
    );
  }

  Widget _buildLoadingBody(){
    return GridView.builder(
        padding: EdgeInsets.all(8),
        itemCount: 6,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: kCityGridItemVerticalSpacing,
          mainAxisSpacing: kCityGridItemVerticalSpacing
        ),
        itemBuilder: (c, i){
          return Shimmer.fromColors(
            baseColor: Colors.grey[300].withOpacity(1),
            highlightColor: Colors.grey[100].withOpacity(1),
            child: Container(
              color: Colors.black,
              height: kCityGridItemMinHeight
            ),
          );
        }
    );
  }

  double calcHeight(){
    int rows = 2;
    if(controller.dashboard?.topCities != null && controller.dashboard.topCities.isNotEmpty){
      if(controller.dashboard.topCities.length <= 3 ){
        rows = 1;
      }
    }
    double height = ((kCityGridItemMinHeight + kCityGridItemVerticalSpacing) * rows) + kCityGridItemVerticalSpacing;
    if(controller.pageState == PageState.loading || (controller.pageState == PageState.loaded && controller.dashboard.topCities.isNotEmpty)){
      return height;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Top Cities", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              FlatButton(
                  padding: EdgeInsets.zero,
                  onPressed: controller.pageState == PageState.loading ? null : (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (c)=> ListCityScreen()
                        )
                    );
                  },
                  child: Text("View all")
              )
            ]
        ),
        SizedBox(
          height: calcHeight(),
          child: controller.pageState == PageState.loading ?
          _buildLoadingBody() : _buildBody(context)
        )
      ],
    );
  }

}
