import 'package:com/components/college_grid_item.dart';
import 'package:com/controllers/dashboard_controller.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/college/top_colleges_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class HomeCollegesSection extends StatelessWidget {
  DashboardController controller;

  HomeCollegesSection(this.controller);

  Widget _buildBody(context) {
    int len = controller.dashboard.topColleges.length > 4
        ? 4
        : controller.dashboard.topColleges.length;
    if (len == 0) {
      return Container(
        alignment: Alignment.center,
        child: Text("No colleges yet",
            style: TextStyle(color: Theme.of(context).primaryColor)),
      );
    }
    return Wrap(
      direction: Axis.horizontal,
      children: controller.dashboard.topColleges.map((college){
        return Container(
          margin: EdgeInsets.only(right: controller.dashboard.topColleges.indexOf(college) % 2 != 0 ? 0 : kCollegeGridItemVerticalSpacing, top: kCollegeGridItemVerticalSpacing),
          width: (MediaQuery.of(context).size.width/2) - (1.5*kCollegeGridItemVerticalSpacing),
          child: CollegeGridItem(
            college: college,
            width: (MediaQuery.of(context).size.width/2) - (1.5*kCollegeGridItemVerticalSpacing),
          ),
        );
      }).toList(),
    );
    // return GridView.builder(
    //     itemCount: len,
    //     shrinkWrap: true,
    //     physics: NeverScrollableScrollPhysics(),
    //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
    //         crossAxisCount: 2,
    //         crossAxisSpacing: kCollegeGridItemVerticalSpacing,
    //         mainAxisSpacing: kCollegeGridItemVerticalSpacing),
    //     itemBuilder: (c, index) {
    //       return CollegeGridItem(
    //         college: controller.dashboard.topColleges[index],
    //       );
    //     });
  }

  Widget _buildLoadingBody() {
    return GridView.builder(
        padding: EdgeInsets.all(8),
        itemCount: 4,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: kCollegeGridItemVerticalSpacing,
            mainAxisSpacing: kCollegeGridItemVerticalSpacing),
        itemBuilder: (c, i) {
          return Shimmer.fromColors(
            baseColor: Colors.grey[300].withOpacity(1),
            highlightColor: Colors.grey[100].withOpacity(1),
            child: Container(
                color: Colors.black, height: kCollegeGridItemMinHeight),
          );
        });
  }

  double calcHeight() {
    int rows = 2;
    if (controller.dashboard?.topColleges != null &&
        controller.dashboard.topColleges.isNotEmpty) {
      if (controller.dashboard.topColleges.length <= 2) {
        rows = 1;
      }
    }
    double height =
        ((kCollegeGridItemMinHeight + kCollegeGridItemVerticalSpacing) * rows) +
            kCollegeGridItemVerticalSpacing + 20;
    if (controller.pageState == PageState.loading ||
        (controller.pageState == PageState.loaded &&
            controller.dashboard.topColleges.isNotEmpty)) {
      return height;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Top Colleges", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              FlatButton(
                padding: EdgeInsets.zero,
                onPressed: controller.pageState == PageState.loading
                    ? null
                    : () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (c) => TopCollegesScreen()));
                      },
                child: Text("View all")
              )
            ]
        ),
        SizedBox(height: 15),
        SizedBox(
            height: calcHeight(),
            child: controller.pageState == PageState.loading
                ? _buildLoadingBody()
                : _buildBody(context))
      ],
    );
  }
}
