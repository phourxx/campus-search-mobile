import 'package:com/components/app_scaffold.dart';
import 'package:com/components/colleges_list.dart';
import 'package:com/controllers/college/college_controller.dart';
import 'package:com/models/account/user.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/constants.dart';
import 'package:com/views/college/filter_screen.dart';
import 'package:com/views/onboarding/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class PartnerDashboardView{
  onUnauthenticated();
}

class PartnerDashboardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ExploreCollegesState();
}

class _ExploreCollegesState extends State<PartnerDashboardScreen>  implements PartnerDashboardView {
  CollegeController controller;
  Filter filter;
  User user;

  @override
  void initState() {
    controller = CollegeController(this);
    controller.load();
    filter = Filter();
    user = User.get();
    super.initState();
  }

  Widget _buildBody(_) {
    return SafeArea(
        bottom: false,
        left: false,
        right: false,
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              _buildHeader(),
              Expanded(
                child: StreamBuilder<PageState>(
                  initialData: PageState.noData,
                  stream: controller.pageStateStream,
                  builder: (c, snapshot) {
                    return CollegeList(
                      pagesState: snapshot.data, colleges: controller.colleges, onRetry: controller.load
                    );
                  },
                )
              )
            ],
          ),
        ));
  }

  Widget _buildHeader() {
    return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ])),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text("Hi ${user.name.split(' ')[0]}",
                      style: Theme.of(context).textTheme.title.copyWith(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      )
                    )
                  ),
                  SizedBox(
                    width: 25,
                    child: FlatButton(
                      padding: EdgeInsets.zero,
                      child: Image.asset("assets/images/filter_icon.png",
                          width: 20, height: 20, fit: BoxFit.contain),
                      onPressed: () async {
                        Filter _filter = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (c) => FilterScreen(filter)));
                        filter = _filter;
                        controller.filterResults(filter);
                      }
                    )
                  ),
                  SizedBox(width: 10),
                  SizedBox(
                    width: 25,
                    child: FlatButton(
                      padding: EdgeInsets.zero,
                      child: Icon(Icons.exit_to_app, color: Colors.white),
                      onPressed: () async {
                        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                        sharedPreferences.remove(USER_DATA_PREF);
                        Navigator.push(context, MaterialPageRoute(
                            builder: (c)=> LoginScreen()
                        ));
                      }
                    )
                  ),
                ],
              ),
              Container(
                color: Colors.black.withOpacity(.1),
                padding: EdgeInsets.symmetric(horizontal: 10),
                margin: EdgeInsets.symmetric(vertical: 15),
                child: Row(children: <Widget>[
                  Icon(
                    Icons.search,
                    color: Colors.white.withOpacity(.5),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                      child: TextField(
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      fillColor: Colors.transparent,
                      focusColor: Colors.transparent,
                      border: InputBorder.none,
                      hintText: "Search colleges",
                      hintStyle: TextStyle(color: Colors.white.withOpacity(.5)),
                    ),
                    onChanged: (value) {
                      controller.filterResults(filter, query: value);
                    },
                  ))
                ]),
              )
            ]));
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      // state: AppState(
      //   pageState: controller.pageState
      // ),
      builder: _buildBody,
    );
  }

  @override
  onUnauthenticated() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(USER_DATA_PREF);
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
        builder: (c)=> LoginScreen()
    ), (r)=> r == null);
  }
}
