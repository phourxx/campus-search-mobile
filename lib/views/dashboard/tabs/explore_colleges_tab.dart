import 'package:com/components/app_scaffold.dart';
import 'package:com/components/colleges_list.dart';
import 'package:com/controllers/college/college_controller.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/college/filter_screen.dart';
import 'package:flutter/material.dart';

class ExploreCollegesTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ExploreCollegesState();
}

class _ExploreCollegesState extends State<ExploreCollegesTab> {
  CollegeController controller;
  Filter filter;

  @override
  void initState() {
    controller = CollegeController();
    controller.load();
    filter = Filter();
    super.initState();
  }

  Widget _buildBody(_) {
    return SafeArea(
        bottom: false,
        left: false,
        right: false,
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              _buildHeader(),
              Expanded(
                child: StreamBuilder<PageState>(
                  initialData: PageState.noData,
                  stream: controller.pageStateStream,
                  builder: (c, snapshot) {
                    return CollegeList(
                      pagesState: snapshot.data, colleges: controller.colleges, onRetry: controller.load,
                    );
                  },
                )
              )
            ],
          ),
        ));
  }

  Widget _buildHeader() {
    return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ])),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Explore Colleges",
                      style: Theme.of(context).textTheme.title.copyWith(
                          fontSize: 23,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                  Spacer(),
                  FlatButton(
                      padding: EdgeInsets.zero,
                      child: Image.asset("assets/images/filter_icon.png",
                          width: 20, height: 20, fit: BoxFit.contain),
                      onPressed: () async {
                        Filter _filter = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (c) => FilterScreen(filter)));
                        filter = _filter;
                        controller.filterResults(filter);
                      }),
                ],
              ),
              Container(
                color: Colors.black.withOpacity(.1),
                padding: EdgeInsets.symmetric(horizontal: 10),
                margin: EdgeInsets.symmetric(vertical: 15),
                child: Row(children: <Widget>[
                  Icon(
                    Icons.search,
                    color: Colors.white.withOpacity(.5),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                      child: TextField(
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      fillColor: Colors.transparent,
                      focusColor: Colors.transparent,
                      border: InputBorder.none,
                      hintText: "Search colleges",
                      hintStyle: TextStyle(color: Colors.white.withOpacity(.5)),
                    ),
                    onChanged: (value) {
                      controller.filterResults(filter, query: value);
                    },
                  ))
                ]),
              )
            ]));
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      // state: AppState(
      //   pageState: controller.pageState
      // ),
      builder: _buildBody,
    );
  }
}
