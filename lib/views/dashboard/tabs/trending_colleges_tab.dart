import 'package:com/components/app_scaffold.dart';
import 'package:com/components/colleges_list.dart';
import 'package:com/controllers/college/college_controller.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/college/filter_screen.dart';
import 'package:flutter/material.dart';

class TrendingCollegesTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TrendingCollegesState();
}

class _TrendingCollegesState extends State<TrendingCollegesTab> {
  CollegeController controller;
  Filter filter;

  @override
  void initState() {
    controller = CollegeController();
    controller.load(trending: true);
    filter = Filter();
    super.initState();
  }

  Widget _buildHeader() {
    return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ])),
        child: Row(
          children: <Widget>[
            Text("Trending Colleges",
                style: Theme.of(context).textTheme.title.copyWith(
                    fontSize: 23,
                    color: Colors.white,
                    fontWeight: FontWeight.bold)),
            Spacer(),
            FlatButton(
                padding: EdgeInsets.zero,
                child: Image.asset("assets/images/filter_icon.png",
                    width: 20, height: 20, fit: BoxFit.contain),
                onPressed: () async {
                  Filter _filter = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (c) => FilterScreen(filter)));
                  filter = _filter;
                  controller.filterResults(filter);
                }),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        builder: (_) => SafeArea(
          left: false, right: false, bottom: false,
          child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                _buildHeader(),
                Expanded(child: StreamBuilder<PageState>(
                  initialData: PageState.loading,
                  stream: controller.pageStateStream,
                  builder: (c, snapshot) {
                    return CollegeList(
                      pagesState: snapshot.data,
                      colleges: controller.colleges,
                      onRetry: ()=> controller.load(trending: true),
                    );
                  }
                ))
              ],
            )
          )
        )
    );
  }
}
