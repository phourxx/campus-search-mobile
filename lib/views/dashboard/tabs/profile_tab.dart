import 'package:com/models/account/dashboard.dart';
import 'package:com/models/account/user.dart';
import 'package:com/utils/constants.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/account/change_password_screen.dart';
import 'package:com/views/account/edit_profile_screen.dart';
import 'package:com/views/college/post_college.dart';
import 'package:com/views/college/shortlisted_colleges_screen.dart';
import 'package:com/views/onboarding/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileTab extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _ProfileState();

}

class _ProfileState extends State<ProfileTab> {

  User user;

  @override
  void initState() {
    super.initState();
    user = User.get();
  }

  Widget _buildHeader() {
    return Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
        decoration: BoxDecoration(
          color: Theme
            .of(context)
            .primaryColor,
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Theme.of(context).primaryColor,
              Theme.of(context).primaryColorDark,
            ]
          )
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Align(
                child: IconButton(
                  icon: Icon(Icons.edit, color: Colors.white),
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (c)=> EditProfileScreen()
                      )
                    );
                  }
                ),
                alignment: Alignment.centerRight,
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: 60,
                    height: 60,
                    margin: EdgeInsets.only(right: 15),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      border: Border.all(color: Colors.white),
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: isEmpty(user.avatar) ? AssetImage("assets/images/user_avatar.png") : NetworkImage(user.avatar)
                      )
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(user.name ?? "-",
                        style: Theme
                            .of(context)
                            .textTheme
                            .title
                            .copyWith(
                            fontSize: 21,
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                        )
                      ),
                      SizedBox(height: 10),
                      Text(user.phone ?? "-",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white
                        )
                      ),
                    ]
                  )
                ],
              )
            ]
        )
    );
  }

  Widget _buildShortlistedAction(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      margin: EdgeInsets.all(20),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Color(0xFFDFDFDF)),
        borderRadius: BorderRadius.all(Radius.circular(5))
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.bookmark, color: Theme.of(context).primaryColor, size: 32),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Shortlisted Colleges",
                  style: Theme
                      .of(context)
                      .textTheme
                      .title
                      .copyWith(
                      fontSize: 17,
                      fontWeight: FontWeight.w500
                  )
              ),
              SizedBox(height: 10),
              Text("${Dashboard.persistence().bookmarkedColleges.length} College in list",
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xff717171),
                  fontWeight: FontWeight.w500
                )
              ),
            ]
          ),
          Spacer(),
          Icon(Icons.chevron_right)
        ]
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false, left: false, right: false,
      child: Container(
        color: Colors.white,
        child: ListView(
          children: <Widget>[
            _buildHeader(),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(
                  builder: (c)=> ShortlistedCollegesScreen()
                ));
              },
              child: _buildShortlistedAction()
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 15),
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xFFDFDFDF)),
                borderRadius: BorderRadius.all(Radius.circular(5))
              ),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.list, color: Colors.black),
                    title: Text("List your college"),
                    trailing: Icon(Icons.chevron_right, color: Colors.black),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (c)=> PostCollegeScreen()
                          )
                      );
                    },
                  ),
                  Divider(
                    color: Color(0xFFDFDFDF),
                    indent: 15,
                    endIndent: 15,
                  ),
                  ListTile(
                    leading: Icon(Icons.lock_outline, color: Colors.black),
                    title: Text("Change Password"),
                    trailing: Icon(Icons.chevron_right, color: Colors.black),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (c)=> ChangePasswordScreen()
                      ));
                    },
                  ),
                  Divider(
                    color: Color(0xFFDFDFDF),
                    indent: 15,
                    endIndent: 15,
                  ),
                  ListTile(
                    leading: Icon(Icons.exit_to_app, color: Colors.red),
                    title: Text("Log Out", style: TextStyle(color: Colors.red)),
                    onTap: () async{
                      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                      sharedPreferences.remove(USER_DATA_PREF);
                      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                          builder: (c)=> LoginScreen()
                      ), (r)=> r == null);
                    }
                  ),
                ]
              )
            )
          ]
        ),
      ),
    );
  }

}