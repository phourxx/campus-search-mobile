import 'package:com/components/app_scaffold.dart';
import 'package:com/controllers/dashboard_controller.dart';
import 'package:com/models/account/user.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/college/shortlisted_colleges_screen.dart';
import 'package:com/views/dashboard/dashboard_screen.dart';
import 'package:com/views/dashboard/notifications_screen.dart';
import 'package:com/views/dashboard/sections/home_cities_section.dart';
import 'package:com/views/dashboard/sections/home_colleges_section.dart';
import 'package:com/views/dashboard/sections/home_streams_section.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:image_fade/image_fade.dart';
import 'package:url_launcher/url_launcher.dart';


class HomeTab extends StatefulWidget{

  DashboardController controller;
  DashboardView dashboardView;

  HomeTab(this.dashboardView, this.controller);

  @override
  State<StatefulWidget> createState() => _HomeState();

}

class _HomeState extends State<HomeTab>{

  DashboardController get controller => widget.controller;
  User user;

  @override
  void initState() {
    super.initState();
    user = User.get();
  }

  Widget _buildBody(_){
    return SafeArea(bottom: false, left: false, right: false, child: Container(
      color: Colors.white,
      child: RefreshIndicator(
        child: ListView(
          children: <Widget>[
            _buildHeader(),
            _buildContent()
          ],
        ),
        onRefresh: () async{
          widget.controller.load();
        }
      ),
    ));
  }

  Widget _buildLoading(_){
    return SafeArea(bottom: false, left: false, right: false, child: Container(
      color: Colors.white,
      child: ListView(
        children: <Widget>[
          _buildHeaderLoading(),
          _buildContent()
        ],
      ),
    ));
  }

  Widget _buildHeader(){
    return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ]
            )
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Hi ${user.name.split(' ')[0]}",
                      style: Theme.of(context).textTheme.title.copyWith(
                          fontSize: 23,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      )
                  ),
                  Spacer(),
                  IconButton(
                      icon: Icon(Icons.bookmark_border),
                      tooltip: "Shortlisted Colleges",
                      color: Colors.white,
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (c)=> ShortlistedCollegesScreen()
                          )
                        );
                      }
                  ),
                  // IconButton(
                  //     tooltip: "Notifications",
                  //     color: Colors.white,
                  //     icon: Icon(Icons.notifications_none),
                  //     onPressed: (){
                  //       Navigator.push(
                  //         context,
                  //         MaterialPageRoute(
                  //           builder: (c)=> NotificationsScreen()
                  //         )
                  //       );
                  //     }
                  // ),
                ],
              ),
              Text("Explore Colleges",
                  style: Theme.of(context).textTheme.title.copyWith(
                      fontSize: 23,
                      color: Colors.white,
                      fontWeight: FontWeight.bold
                  )
              ),
              InkWell(
                onTap: ()=> widget.dashboardView.gotoTab(1),
                child: Container(
                  color: Colors.black.withOpacity(.1),
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.search,
                          color: Colors.white.withOpacity(.5),
                        ),
                        SizedBox(width: 10),
                        Text("Search colleges", style: TextStyle(color: Colors.white.withOpacity(.5)))
                      ]
                  ),
                ),
              )
            ]
        )
    );
  }

  Widget _buildHeaderLoading(){
    return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ]
            )
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Shimmer.fromColors(
                    baseColor: Colors.black.withOpacity(.1),
                    highlightColor: Colors.black.withOpacity(.2),
                    child: Container(
                        color: Colors.black,
                        height: 40,
                        width: 100
                    ),
                  ),
                  Spacer(),
                  Shimmer.fromColors(
                    baseColor: Colors.black.withOpacity(.1),
                    highlightColor: Colors.black.withOpacity(.2),
                    child: Container(
                        color: Colors.black,
                        height: 24,
                        width: 24
                    ),
                  ),
                  Shimmer.fromColors(
                    baseColor: Colors.black.withOpacity(.1),
                    highlightColor: Colors.black.withOpacity(.2),
                    child: Container(
                        margin: EdgeInsets.only(left: 10),
                        color: Colors.black,
                        height: 24,
                        width: 24
                    ),
                  ),
                ],
              ),
              Shimmer.fromColors(
                baseColor: Colors.black.withOpacity(.1),
                highlightColor: Colors.black.withOpacity(.2),
                child: Container(
                    margin: EdgeInsets.only(top: 10),
                    color: Colors.black,
                    height: 40,
                    width: 150
                ),
              ),
              Shimmer.fromColors(
                baseColor: Colors.black.withOpacity(.1),
                highlightColor: Colors.black.withOpacity(.2),
                child: Container(
                    margin: EdgeInsets.only(top: 10),
                    color: Colors.black,
                    height: 40,
                    width: double.infinity
                ),
              )
            ]
        )
    );
  }

  Widget _buildBanner(){
    return StreamBuilder<int>(
      initialData: 0,
      stream: controller.bannerCounterStream,
      builder: (c, snapshot){
        return SizedBox(
          height: 150,
          child: InkWell(
            onTap: () async{
              String url = controller.dashboard.banners[snapshot.data].url;
              if (await canLaunch(url)) {
                await launch(url);
              } else {
                showError(context, "Unable to open webpage");
              }
            },
            child: controller.banners.isNotEmpty ? ImageFade(
              image: controller.banners[snapshot.data],
              placeholder: Container(
                color: Color(0xfff1f1f1),
                child: Center(child: Icon(Icons.photo, color: Colors.white30, size: 70.0,)),
              ),
              alignment: Alignment.center,
              fit: BoxFit.fill,
              loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent event) {
                if (event == null) { return child; }
                return Center(
                  child: CircularProgressIndicator(
                    value: event.expectedTotalBytes == null ? 0.0 : event.cumulativeBytesLoaded / event.expectedTotalBytes
                  ),
                );
              },
              errorBuilder: (BuildContext context, Widget child, dynamic exception) {
                return Container(
                  color: Color(0xfff1f1f1),
                  child: Center(child: Icon(Icons.warning, color: Colors.black26, size: 70.0)),
                );
              },
            ) : Container(
              color: Color(0xfff1f1f1),
              height: 150
            )
          )
        );
      },
    );
  }

  Widget _buildContent(){
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          _buildBanner(),
          SizedBox(height: 20),
          HomeStreamsSection(controller),
          // SizedBox(height: 20),
          // HomeCitiesSection(controller),
          SizedBox(height: 20),
          HomeCollegesSection(controller),
        ]
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PageState>(
      initialData: PageState.loading,
      stream: controller.pageStateStream,
      builder: (c, snapshot){
        return AppScaffold(
          state: AppState(
            pageState: snapshot.data,
            onRetry: (){
              widget.controller.load();
              // setState(() {});
            }
          ),
          loadingBuilder: _buildLoading,
          builder: _buildBody,
        );
      },
    );
  }

}