import 'package:com/components/rounded_button.dart';
import 'package:com/services/auth_service.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/onboarding/login_screen.dart';
import 'package:flutter/material.dart';

class SetPasswordScreen extends StatefulWidget{
  String phone;

  SetPasswordScreen(this.phone);

  @override
  State<StatefulWidget> createState() => _SetPasswordScreenState();

}

class _SetPasswordScreenState extends State<SetPasswordScreen>{

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool showPassword = false;
  bool showPassword2 = false;
  bool isBusy = false;
  String password;
  String password2;

  void submit() async {
    if(formKey.currentState.validate()){
      setState((){
        isBusy = true;
      });
      AuthService().resetPassword({"phone": widget.phone, 'password': password}).then((value){
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
          builder: (c)=> LoginScreen()
        ), (r)=> r == null);
      }).catchError((e){
        showError(context, e['message']);
      }).whenComplete((){
        setState((){
          isBusy = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/login_bg.png"),
                    fit: BoxFit.fill
                )
            ),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 30),
                Align(
                  child: FlatButton(
                    padding: EdgeInsets.zero,
                    onPressed: (){},
                    child: Icon(Icons.chevron_left, size: 30)
                  ),
                  alignment: Alignment.centerLeft,
                ),
                SizedBox(height: 50),
                Container(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Set\nPassword",
                          style: Theme.of(context).textTheme.title.copyWith(
                            fontSize: 30, fontWeight: FontWeight.bold
                          )
                        ),
                        SizedBox(height: 60),
                        Form(
                          key: formKey,
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                obscureText: !showPassword,
                                decoration: InputDecoration(
                                  hintText: "New Password",
                                  labelText: "New Password",
                                  suffix: IconButton(
                                      padding: EdgeInsets.zero,
                                      onPressed: ()=> setState((){
                                        showPassword = !showPassword;
                                      }),
                                      icon: Icon(!showPassword ? Icons.visibility_off : Icons.visibility)
                                  )
                                ),
                                validator: (value){
                                  if(value.isEmpty){
                                    return "this field is required";
                                  }
                                  if(value.length < 6 || value.length > 12){
                                    return "password should be 6 to 12 characters";
                                  }
                                  return null;
                                },
                                onChanged: (value){
                                  setState((){
                                    password = value;
                                  });
                                }
                              ),
                              SizedBox(height: 20),
                              TextFormField(
                                obscureText: !showPassword2,
                                decoration: InputDecoration(
                                  hintText: "Confirm Password",
                                  labelText: "Confirm Password",
                                  suffix: IconButton(
                                      padding: EdgeInsets.zero,
                                    onPressed: ()=> setState((){
                                      showPassword2 = !showPassword2;
                                    }),
                                    icon: Icon(!showPassword2 ? Icons.visibility_off : Icons.visibility)
                                  )
                                ),
                                validator: (value){
                                  if(value.isEmpty){
                                    return "this field is required";
                                  }
                                  if(value.length < 6 || value.length > 12){
                                    return "password should be 6 to 12 characters";
                                  }
                                  return null;
                                },
                                onChanged: (value){
                                  setState((){
                                    password2 = value;
                                  });
                                }
                              ),
                            ],
                          )
                        ),
                        SizedBox(height: 40),
                        RoundedButton(
                          label: "Reset Password",
                          onTap: submit,
                          isLoading: isBusy,
                          disabled: isEmpty(password) || isEmpty(password2) || password != password2,
                        )
                      ]
                    )
                ),
                SizedBox(height: 30)
              ],
            )
        )
    );
  }

}