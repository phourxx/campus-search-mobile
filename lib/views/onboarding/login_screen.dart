import 'dart:convert';

import 'package:com/components/rounded_button.dart';
import 'package:com/models/account/user.dart';
import 'package:com/services/auth_service.dart';
import 'package:com/utils/constants.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/college/post_college.dart';
import 'package:com/views/dashboard/dashboard_screen.dart';
import 'package:com/views/dashboard/partner_dashboard_screen.dart';
import 'package:com/views/onboarding/forgot_password_screen.dart';
import 'package:com/views/onboarding/otp_screen.dart';
import 'package:com/views/onboarding/register_screen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _LoginScreenState();

}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController usernameController;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool showPassword = false;
  String uname;
  String password;
  bool busy = false;

  @override
  void initState() {
    super.initState();
  }


  Future<void> logInGoogle() async {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: [
        'email',
        // you can add extras if you require
      ],
    );
    try{
      if(_googleSignIn.currentUser != null){
        _googleSignIn.disconnect().then((value){
          _googleSignIn.signIn().then((GoogleSignInAccount acc) async {
            await acc.authentication;
            print(acc.id);
            print(acc.email);
            print(acc.displayName);
            print(acc.photoUrl);
            doOAuthLogin({"email": acc.email, 'name': acc.displayName, "password": acc.email, "avatar": acc.photoUrl, "token": acc.id, 'user_type': 'google'});

            // acc.authentication.then((GoogleSignInAuthentication auth) async {
            //   print(auth.idToken);
            //   print(auth.accessToken);
            // });
          });
        });
      }else{
        _googleSignIn.signIn().then((GoogleSignInAccount acc) async {
            await acc.authentication;
            print(acc.id);
            print(acc.email);
            print(acc.displayName);
            print(acc.photoUrl);
            doOAuthLogin({"email": acc.email, 'name': acc.displayName, "password": acc.email, "avatar": acc.photoUrl, "token": acc.id, 'user_type': 'google'});

            // acc.authentication.then((GoogleSignInAuthentication auth) async {
            //   print(auth.idToken);
            //   print(auth.accessToken);
            // });
          });
      }
    }catch(e){
      print(e);
      showError(context, "Sorry, an error occurred. Please try another login method");
    }
  }

  void doOAuthLogin(dynamic data){
    setState(() {
      busy = true;
    });
    AuthService().oAuthLogin(data).then((user) async{
      user.save();
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString(USER_DATA_PREF, jsonEncode(user.toJson()));
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (c)=> user.isPartner ? PartnerDashboardScreen() : DashboardScreen()
        ),
        (r)=> r == null
      );
    }).catchError((error){
      if(error.runtimeType == String){
        showError(context, error);
      }else{
        if(error['reason'] == 'not_verified'){
          Navigator.push(context, MaterialPageRoute(
            builder: (c)=> OtpScreen(
              title: "Phone\nVerification.",
              phone: uname,
              type: "verify",
              subtitle: "An otp code was sent to your Phone. Please enter the code",
              onSubmit: (scaffoldKey, con) async{
                Navigator.pop(con);
                Navigator.pop(context);
                User user = User.fromJson(error['user']);
                user.phoneVerified = true;
                user.save();
                SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                sharedPreferences.setString(USER_DATA_PREF, jsonEncode(user.toJson()));
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (c)=> user.isPartner ? PartnerDashboardScreen() : DashboardScreen()
                  ),
                  (r)=> r == null
                );
              },
            )
          ));
        }else{
          showError(context, error['message']);
        }
      }
    }).whenComplete(() => setState(() => busy = false));
  }

  void submit() async{
    if(formKey.currentState.validate()){
      formKey.currentState.save();
      setState(() {
        busy = true;
      });
      AuthService().login(
        {"email": uname, "password": password}
      ).then((user) async{
        user.save();
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        sharedPreferences.setString(USER_DATA_PREF, jsonEncode(user.toJson()));
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (c)=> user.isPartner ? PartnerDashboardScreen() : DashboardScreen()
          ),
          (r)=> r == null
        );
      }).catchError((error){
        if(error.runtimeType == String){
          showError(context, error);
        }else{
          if(error['reason'] == 'not_verified'){
            Navigator.push(context, MaterialPageRoute(
              builder: (c)=> OtpScreen(
                title: "Phone\nVerification.",
                phone: uname,
                type: "verify",
                subtitle: "An otp code was sent to your Phone. Please enter the code",
                onSubmit: (scaffoldKey, con) async{
                  Navigator.pop(con);
                  Navigator.pop(context);
                  User user = User.fromJson(error['user']);
                  user.phoneVerified = true;
                  user.save();
                  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                  sharedPreferences.setString(USER_DATA_PREF, jsonEncode(user.toJson()));
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (c)=> user.isPartner ? PartnerDashboardScreen() : DashboardScreen()
                    ),
                    (r)=> r == null
                  );
                },
              )
            ));
          }else{
            showError(context, error['message']);
          }
        }
      }).whenComplete(() => setState(() => busy = false));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/login_bg.png"),
            fit: BoxFit.fill
          )
        ),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 30),
            Align(
              child: FlatButton(
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (c)=> PostCollegeScreen()
                    )
                  );
                },
                textColor: Theme.of(context).primaryColor,
                child: Text("Post College")
              ),
              alignment: Alignment.centerRight,
            ),
            SizedBox(height: 30),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Log in",
                      style: Theme.of(context).textTheme.title.copyWith(
                        fontSize: 30, fontWeight: FontWeight.bold
                      )
                    )
                  ),
                  SizedBox(height: 30),
                  Form(
                    key: formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: usernameController,
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                            hintText: "xxxxxxxxxx",
                            labelText: "Phone",
                            prefix: Text("+91")
                          ),
                          validator: (value){
                            if(value.isEmpty){
                              return "this field is required";
                            }
                            if(value.trim().length != 10){
                              return "enter a valid phone number";
                            }
                            return null;
                          },
                          onSaved: (value)=> uname=value,
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          obscureText: !showPassword,
                          decoration: InputDecoration(
                            hintText: "Password",
                            labelText: "Password",
                            suffix: IconButton(
                              padding: EdgeInsets.zero,
                              onPressed: ()=> setState((){
                                showPassword = !showPassword;
                              }),
                              icon: Icon(!showPassword ? Icons.visibility_off : Icons.visibility, color: Theme.of(context).primaryColor)
                            )
                          ),
                          validator: (value){
                            if(value.isEmpty){
                              return "this field is required";
                            }
                            if(value.length < 6 || value.length > 12){
                              return "password should be 6 to 12 characters";
                            }
                            return null;
                          },
                          onSaved: (value)=> password=value
                        ),
                      ],
                    )
                  ),
                  SizedBox(height: 15),
                  Align(
                    child: FlatButton(
                      padding: EdgeInsets.zero,
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (c)=> ForgotPasswordScreen()
                          )
                        );
                      },
                      child: Text("Forgot password")
                    ),
                    alignment: Alignment.centerRight,
                  ),
                  SizedBox(height: 15),
                  RoundedButton(
                    label: "Login",
                    onTap: submit,
                    isLoading: busy,
                  ),
                  SizedBox(height: 40),
                  Text(
                    "OR"
                  ),
                  SizedBox(height: 15),
                  GoogleSignInButton(
                    onPressed: logInGoogle,
                    darkMode: true, // default: false
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: <Widget>[
                  //     FlatButton(
                  //         onPressed: logInGoogle,
                  //         padding: EdgeInsets.zero,
                  //         child: Image.asset("assets/images/google_icon.png", width: 50, height: 50)
                  //     ),
                  //     FlatButton(
                  //         onPressed: logInFacebook,
                  //         padding: EdgeInsets.zero,
                  //         child: Image.asset("assets/images/fb_icon.png", width: 50, height: 50)
                  //     ),
                  //   ],
                  // ),
                  SizedBox(height: 30),
                  RichText(
                      text: TextSpan(
                          text: "Don't have any account, ",
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                                text: "Sign up",
                                style: TextStyle(decoration: TextDecoration.underline, color: Colors.black),
                                recognizer: TapGestureRecognizer()..onTap = (){
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (c)=> RegisterScreen()
                                  ));
                                }
                            )
                          ]
                      )
                  )
                ]
              )
            ),
            SizedBox(height: 30)
          ],
        )
      )
    );
  }

}