
import 'package:com/components/rounded_button.dart';
import 'package:com/services/auth_service.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/dashboard/dashboard_screen.dart';
import 'package:com/views/onboarding/otp_screen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RegisterScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _RegisterScreenState();

}

class _RegisterScreenState extends State<RegisterScreen> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool showPassword = false;
  TextEditingController groupController = TextEditingController();
  String selectedGroup, name, phone, password;
  bool busy = false;
  List<String> groups = [
    "Student",
    "Parents",
    "Counselor",
    "Partner",
    "College staff"
  ];

  void submit() async{
    if(formKey.currentState.validate()){
      formKey.currentState.save();
      setState(() {
        busy = true;
      });
      AuthService().register(
        {"phone": phone, 'email': phone, "password": password, 'name': name, 'group': selectedGroup}
      ).then((user){
        Navigator.push(context, MaterialPageRoute(
          builder: (c)=> OtpScreen(
            title: "Phone\nVerification.",
            phone: phone,
            type: "verify",
            subtitle: "An otp code was sent to your Phone. Please enter the code",
            onSubmit: (scaffoldKey, con){
              Navigator.pop(con);
              Navigator.pop(context);
              user.phoneVerified = true;
              user.save();
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (c)=> DashboardScreen()
                ),
                (r)=> r == null
              );
            },
          )
        ));
      }).catchError((error){
        String err = "";
        if(error.runtimeType == String){
          err = error;
        }else{
          if(error["errors"] != null){
            error["errors"].forEach((key, value) {
              if(key != "email"){
                err += "${value.join('\n')}\n";
              }
            });
          }else{
            err = error['message'];
          }
        }
        showError(context, err);
      }).whenComplete(() => setState(() => busy = false));
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/signup_bg.png"),
            fit: BoxFit.fill
          )
        ),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 100),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Sign Up",
                      style: Theme.of(context).textTheme.title.copyWith(
                        color: Colors.white,
                        fontSize: 30, fontWeight: FontWeight.bold
                      )
                    )
                  ),
                  SizedBox(height: 30),
                  Form(
                      key: formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            keyboardType: TextInputType.text,
                            style: TextStyle(color: Colors.white),
                            onChanged: (value){
                              setState(() {
                                name = value;
                              });
                            },
                            validator: (value){
                              if(value.isEmpty){
                                return "this field is required";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "Your Name",
                              labelText: "Name",
                              labelStyle: TextStyle(color: Colors.white),
                              hintStyle: TextStyle(color: Colors.white70),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white, width: 1)
                              )
                            ),
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            keyboardType: TextInputType.phone,
                            style: TextStyle(color: Colors.white),
                            onChanged: (value){
                              setState(() {
                                phone = value;
                              });
                            },
                            validator: (value){
                              if(value.isEmpty){
                                return "this field is required";
                              }
                              if(value.trim().length != 10){
                                return "enter a valid phone number";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "xxxxxxxxxx",
                              labelText: "Phone",
                              prefix: Text("+91"),
                              labelStyle: TextStyle(color: Colors.white),
                              hintStyle: TextStyle(color: Colors.white70),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white, width: 1)
                              )
                            ),
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            readOnly: true,
                            controller: groupController,
                            style: TextStyle(color: Colors.white),
                            validator: (value){
                              if(value.isEmpty){
                                return "this field is required";
                              }
                              return null;
                            },
                            onTap: (){
                              showAppBottomSheetList<String>(
                                context: context,
                                items: groups,
                                itemBuilder: (String item) {
                                  return ListTile(
                                    title: Text(
                                      item,
                                    ),
                                  );
                                },
                                onItemSelected: (item) {
                                  selectedGroup = item;
                                  groupController.text = item;
                                  setState(() {});
                                },
                                title: "Tell us who you are",
                                hasSearch: true,
                                searchMatcher: (String item, String text) {
                                  return item
                                      .toLowerCase()
                                      .contains(text.toLowerCase());
                                },
                              );
                            },
                            decoration: InputDecoration(
                              hintText: "Tell us who you are",
                              labelText: "Tell us who you are",
                              suffixIcon: Icon(Icons.keyboard_arrow_down, color: Colors.white),
                              labelStyle: TextStyle(color: Colors.white),
                              hintStyle: TextStyle(color: Colors.white70),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white, width: 1)
                              )
                            )
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            obscureText: !showPassword,
                            style: TextStyle(color: Colors.white),
                            onChanged: (value){
                              setState(() {
                                password = value;
                              });
                            },
                            validator: (value){
                              if(value.isEmpty){
                                return "this field is required";
                              }
                              if(value.length < 6 || value.length > 12){
                                return "password should be 6 to 12 characters";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "Your Password",
                              labelText: "Password",
//                              suffix: FlatButton(
//                                padding: EdgeInsets.zero,
//                                onPressed: ()=> setState((){
//                                  showPassword = !showPassword;
//                                }),
//                                child: Text(
//                                  showPassword ? "hide" : "show",
//                                  style: TextStyle(color: Colors.white),
//                                )
//                              ),
                              labelStyle: TextStyle(color: Colors.white),
                              hintStyle: TextStyle(color: Colors.white70),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white70, width: 1)
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white, width: 1)
                              )
                            ),
                          ),
                        ],
                      )
                  ),
                  SizedBox(height: 35),
                  Align(
                    child: RichText(
                      text: TextSpan(
                        text: "By signing up, you agree to our ",
                        style: TextStyle(color: Colors.white),
                        children: [
                          TextSpan(
                            text: "Terms of service",
                            style: TextStyle(decoration: TextDecoration.underline, color: Colors.white),
                            recognizer: TapGestureRecognizer()..onTap = (){

                            }
                          )
                        ]
                      )
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                  SizedBox(height: 35),
                  RoundedButton(
                    label: "Signup",
                    onTap: submit,
                    isLoading: busy,
                    disabled: isEmpty(name) || isEmpty(phone) || isEmpty(selectedGroup) || isEmpty(password),
                    labelColor: Theme.of(context).primaryColor,
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(height: 40),
                  RichText(
                    text: TextSpan(
                      text: "Already have an account, ",
                      style: TextStyle(color: Colors.white),
                      children: [
                        TextSpan(
                          text: "Login",
                          style: TextStyle(decoration: TextDecoration.underline, color: Colors.white),
                          recognizer: TapGestureRecognizer()..onTap = (){
                            Navigator.pop(context);
                          }
                        )
                      ]
                    )
                  )
                ]
              )
            ),
            SizedBox(height: 30)
          ],
        )
      )
    );
  }

}