import 'package:com/components/rounded_button.dart';
import 'package:com/services/auth_service.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/onboarding/login_screen.dart';
import 'package:com/views/onboarding/otp_screen.dart';
import 'package:com/views/onboarding/set_password_screen.dart';
import 'package:flutter/material.dart';

class ForgotPasswordScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() => _ForgotPasswordScreenState();

}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen>{

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool isBusy = false;
  String phone;

  void submit() async {
    if(formKey.currentState.validate()){
      setState((){
        isBusy = true;
      });
      AuthService().sendOtp({"phone": phone}).then((value){
        Navigator.push(context, MaterialPageRoute(
          builder: (c)=> OtpScreen(
            title: "Enter 4 digit\nRecovery code",
            subtitle: "The recovery code was sent to your Phone. Please enter the code",
            phone: phone,
            type: 'reset',
            onSubmit: (scaffoldKey, con){
              Navigator.pop(con);
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(
                  builder: (c)=> SetPasswordScreen(phone)
              ));
            },
          )
        ));
      }).catchError((e){
        showError(context, e['message']);
      }).whenComplete((){
        setState((){
          isBusy = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/login_bg.png"),
                    fit: BoxFit.fill
                )
            ),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 30),
                Align(
                  child: FlatButton(
                    padding: EdgeInsets.zero,
                    onPressed: ()=> Navigator.pop(context),
                    child: Icon(Icons.chevron_left, size: 30)
                  ),
                  alignment: Alignment.centerLeft,
                ),
                SizedBox(height: 50),
                Container(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Forgot\nPassword",
                          style: Theme.of(context).textTheme.title.copyWith(
                            fontSize: 30, fontWeight: FontWeight.bold
                          )
                        ),
                        SizedBox(height: 30),
                        Text(
                          "Don't worry, just enter your Phone number and we'll set you up a new password in no time !"
                        ),
                        SizedBox(height: 30),
                        Form(
                          key: formKey,
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                keyboardType: TextInputType.phone,
                                decoration: InputDecoration(
                                  hintText: "xxxxxxxxxx",
                                  labelText: "Phone",
                                  prefix: Text("+91")
                                ),
                                validator: (value){
                                  if(value.isEmpty){
                                    return "this field is required";
                                  }
                                  if(value.trim().length != 10){
                                    return "enter a valid phone number";
                                  }
                                  return null;
                                },
                                onChanged: (value){
                                  setState(() {
                                    phone = value;
                                  });
                                },
                              )
                            ],
                          )
                        ),
                        SizedBox(height: 40),
                        RoundedButton(
                          label: "Send",
                          isLoading: isBusy,
                          disabled: isEmpty(phone),
                          onTap: submit,
                        )
                      ]
                    )
                ),
                SizedBox(height: 30)
              ],
            )
        )
    );
  }

}