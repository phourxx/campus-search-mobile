import 'package:com/components/rounded_button.dart';
import 'package:com/services/auth_service.dart';
import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';

class OtpScreen extends StatefulWidget{
  String title;
  String subtitle;
  String phone;
  String type;
  Function onSubmit;

  OtpScreen({this.title, this.subtitle, this.type, this.phone, this.onSubmit});

  @override
  State<StatefulWidget> createState() => _OtpScreenState();

}

class _OtpScreenState extends State<OtpScreen>{

  GlobalKey formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  String otp;
  bool isBusy = false;

  void submit() async {
    setState(() => isBusy = true);
    AuthService().verifyOtp(
      {"phone": widget.phone, "otp": otp, 'type': widget.type}
    ).then((value){
      if(value['valid'] ?? false){
        widget.onSubmit(scaffoldKey, context);
      }else{
        showError(context, "Invalid otp.");
      }
    }).catchError((error){
        if(error.runtimeType == String){
          showError(context, error);
        }else{
          showError(context, error['message']);
        }
      }
    ).whenComplete(() => setState(() => isBusy = false));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: true,
      body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/login_bg.png"),
                  fit: BoxFit.fill
              )
          ),
          child: ListView(
            children: <Widget>[
              SizedBox(height: 30),
              Align(
                child: FlatButton(
                  padding: EdgeInsets.zero,
                  onPressed: ()=> Navigator.pop(context),
                  child: Icon(Icons.chevron_left, size: 30)
                ),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(height: 50),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget.title,
                        style: Theme.of(context).textTheme.title.copyWith(
                          fontSize: 30, fontWeight: FontWeight.bold
                        )
                      ),
                      SizedBox(height: 30),
                      Text(
                        widget.subtitle
                      ),
                      SizedBox(height: 30),
                      PinEntryTextField(
                        onSubmit: (String pin){
                          setState(() {
                            otp = pin;
                          });
                          submit();
                        }, // end onSubmit
                      ),
                      SizedBox(height: 40),
                      RoundedButton(
                        disabled: isEmpty(otp),
                        label: "Submit",
                        isLoading: isBusy,
                        onTap: submit,
                      )
                    ]
                  )
              ),
              SizedBox(height: 30)
            ],
          )
      )
    );
  }

}