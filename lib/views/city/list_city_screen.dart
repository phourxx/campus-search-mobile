import 'package:com/components/app_scaffold.dart';
import 'package:com/components/city_grid_item.dart';
import 'package:com/components/city_list_item.dart';
import 'package:com/controllers/city_controller.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/college/stream_colleges_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ListCityScreen extends StatefulWidget {
  StreamObject stream;
  Course course;

  ListCityScreen([this.stream, this.course]);

  @override
  State<StatefulWidget> createState() => _ListCityScreen();

}

class _ListCityScreen extends State<ListCityScreen> {

  CityController controller;


  @override
  void initState() {
    controller = CityController();
    // FetchCityFilter.all;
    controller.load();
    super.initState();
  }

  Widget _buildList(_){
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: GridView.builder(
        itemCount: controller.cities.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: kCityGridItemVerticalSpacing,
          mainAxisSpacing: kCityGridItemVerticalSpacing
        ),
        itemBuilder: (c, index){
          return CityGridItem(
            city: controller.cities[index],
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (c)=> StreamCollegesScreen(widget.stream, widget.course, controller.cities[index])
                  )
              );
            },
          );
        }
      )
    );
    // return Container(
    //   color: Colors.white,
    //   child: ListView.separated(
    //       padding: EdgeInsets.all(8),
    //       itemCount: controller.cities.length,
    //       shrinkWrap: true,
    //       physics: NeverScrollableScrollPhysics(),
    //       separatorBuilder: (c, i)=> SizedBox(height: 10),
    //       itemBuilder: (context, index){
    //         return CityListItem(controller.cities[index]);
    //       }
    //   )
    // );
  }

  Widget _buildLoadingWidget(c){
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      child: GridView.builder(
        padding: EdgeInsets.all(8),
        itemCount: 12,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: kCityGridItemVerticalSpacing,
          mainAxisSpacing: kCityGridItemVerticalSpacing
        ),
        itemBuilder: (c, i){
          return Shimmer.fromColors(
            baseColor: Colors.grey[300].withOpacity(1),
            highlightColor: Colors.grey[100].withOpacity(1),
            child: Container(
              color: Colors.black,
              height: kCityGridItemMinHeight
            ),
          );
        }
      )
    );
    // return Container(
    //   color: Colors.white,
    //   child: ListView.separated(
    //       padding: EdgeInsets.all(8),
    //       itemCount: 9,
    //       separatorBuilder: (c, i)=> SizedBox(height: 10),
    //       itemBuilder: (c, i){
    //         return Shimmer.fromColors(
    //           baseColor: Colors.grey[300].withOpacity(1),
    //           highlightColor: Colors.grey[100].withOpacity(1),
    //           child: Container(
    //               color: Colors.black,
    //               height: 50
    //           ),
    //         );
    //       }
    //   )
    // );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PageState>(
        stream: controller.loadingStream,
        initialData: PageState.loading,
        builder: (context, snapshot){
          return AppScaffold(
            title: "Select City",
            showAppBar: true,
            state: AppState(
              pageState: snapshot.data,
              hasSearch: true,
              onRetry: controller.load,
              onSearchChanged: controller.filterCities,
              onSearchExit: controller.clearFilter
            ),
            loadingBuilder: _buildLoadingWidget,
            builder: _buildList
          );
        }
    );
  }

}