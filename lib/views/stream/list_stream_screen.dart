import 'package:com/components/app_scaffold.dart';
import 'package:com/components/stream_grid_item.dart';
import 'package:com/controllers/stream_controller.dart';
import 'package:com/services/stream_service.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/college/stream_colleges_screen.dart';
import 'package:com/views/stream/select_stream_screen.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ListStreamScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ListStreamScreen();
}

class _ListStreamScreen extends State<ListStreamScreen> {
  StreamController controller;

  @override
  void initState() {
    controller = StreamController();
    controller.loadStreams(FetchStreamFilter.selected);
    super.initState();
  }

  Widget _buildGrid(_) {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.all(15),
        child: GridView.builder(
            itemCount: controller.streams.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, crossAxisSpacing: 10, mainAxisSpacing: 10),
            itemBuilder: (context, index) {
              return StreamGridItem(
                stream: controller.streams[index],
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (c) =>
                              StreamCollegesScreen(controller.streams[index])));
                },
              );
            }));
  }

  Widget _buildLoadingWidget(c) {
    return Container(
        color: Colors.white,
//        padding: EdgeInsets.all(15),
        child: GridView.builder(
            padding: EdgeInsets.all(8),
            itemCount: 9,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, crossAxisSpacing: 10, mainAxisSpacing: 10),
            itemBuilder: (c, i) {
              return Shimmer.fromColors(
                baseColor: Colors.grey[300].withOpacity(1),
                highlightColor: Colors.grey[100].withOpacity(1),
                child: Container(
                    color: Colors.black, height: kStreamGridItemMinHeight),
              );
            }));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PageState>(
        stream: controller.loadingStream,
        initialData: PageState.loading,
        builder: (context, snapshot) {
          return AppScaffold(
              title: "Your Streams",
              showAppBar: true,
              state: AppState(pageState: snapshot.data),
              actions: <Widget>[
                IconButton(
                    padding: EdgeInsets.zero,
                    tooltip: "Add streams",
                    icon: Icon(Icons.add, color: Colors.white),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (c) => SelectStreamScreen()));
                    })
              ],
              loadingBuilder: _buildLoadingWidget,
              builder: _buildGrid);
        });
  }
}
