import 'package:com/components/app_scaffold.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/city/list_city_screen.dart';
import 'package:flutter/material.dart';

class SelectStreamCourseScreen extends StatefulWidget{
  StreamObject stream;

  SelectStreamCourseScreen({this.stream});

  @override
  State<StatefulWidget> createState() => _SelectStreamCourseScreen();
}

class _SelectStreamCourseScreen extends State<SelectStreamCourseScreen>{
  String query;

  List<Course> getCourses(){
    if(isEmpty(query)){
      return widget.stream.courses;
    }
    return widget.stream.courses.where((course) => course.title.toLowerCase().contains(query.toLowerCase())).toList();
  }

  @override
  Widget build(BuildContext context) {
    var courses = getCourses();
    return AppScaffold(
      title: "Select Course",
      showAppBar: true,
      state: AppState(
        pageState: courses.isNotEmpty ? PageState.loaded : PageState.noData,
        hasSearch: true,
        onSearchChanged: (q)=> setState((){query = q;}),
        onSearchExit: ()=> setState((){query = null;})
      ),
      builder: (c)=> Container(
        color: Colors.white,
        child: ListView.separated(
          itemCount: courses.length,
          itemBuilder: (c, i){
            return ListTile(
              title: Text(courses[i].title),
              trailing: Icon(Icons.chevron_right),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (c)=> ListCityScreen(widget.stream, courses[i])
                    )
                );
              },
            );
          },
          separatorBuilder: (c, i){
            return Divider();
          },
        )
      )
    );
  }
}