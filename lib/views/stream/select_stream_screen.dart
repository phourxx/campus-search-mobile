import 'package:com/components/app_scaffold.dart';
import 'package:com/components/rounded_button.dart';
import 'package:com/components/stream_grid_item.dart';
import 'package:com/controllers/stream_controller.dart';
import 'package:com/services/stream_service.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class SelectStreamScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _SelectStreamScreenState();

}

class _SelectStreamScreenState extends State<SelectStreamScreen> implements SaveStreamsView {
  StreamController controller;


  @override
  void initState() {
    controller = StreamController(this);
    controller.loadStreams(FetchStreamFilter.all);
    super.initState();
  }

  Widget _buildGrid(){
    return GridView.builder(
      itemCount: controller.streams.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10
      ),
      itemBuilder: (context, index){
        return StreamGridItem(
          stream: controller.streams[index],
          onTap: (){
            var streams = controller.streams;
            streams[index].selected = !streams[index].selected;
            controller.streams = streams;
          },
        );
      }
    );
  }

  Widget _buildLoadingWidget(c){
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 40),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                IconButton(icon: Icon(Icons.chevron_left, size: 30), onPressed: (){
                  Navigator.pop(context);
                }),
                Text(
                    "Select Your Stream",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                    )
                ),
              ]
            ),
            SizedBox(height: 20),
            Expanded(child: GridView.builder(
                padding: EdgeInsets.all(8),
                itemCount: 9,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10
                ),
                itemBuilder: (c, i){
                  return Shimmer.fromColors(
                    baseColor: Colors.grey[300].withOpacity(1),
                    highlightColor: Colors.grey[100].withOpacity(1),
                    child: Container(
                        color: Colors.black,
                        height: kStreamGridItemMinHeight
                    ),
                  );
                }
            ))
          ],
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PageState>(
      stream: controller.loadingStream,
      initialData: PageState.loading,
      builder: (context, snapshot){
        return AppScaffold(
            backgroundColor: Colors.white,
            state: AppState(
              pageState: snapshot.data
            ),
            loadingBuilder: _buildLoadingWidget,
            builder: (context) => SafeArea(child: Column(
                children: <Widget>[
                  Expanded(
                      child: ListView(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        children: <Widget>[
                          SizedBox(height: 40),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.chevron_left, size: 30), onPressed: (){
                                Navigator.pop(context);
                              }),
                              Text(
                                  "Select Your Stream",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold
                                  )
                              ),
                            ]
                          ),
                          SizedBox(height: 20),
                          _buildGrid()
                        ],
                      )
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: StreamBuilder(
                      stream: controller.streamListStream,
                      initialData: controller.streams,
                      builder: (context, snapshot){
                        return RoundedButton(
                          isLoading: controller.isSaving,
                          disabled: !controller.canSubmit,
                          onTap: (){
                            controller.saveStreams();
                          },
                          label: "Save",
                        );
                      }
                    )
                  )
                ]
            ))
        );
      }
    );
  }

  @override
  void onError() {
    showError(context, controller.error);
  }

  @override
  void onSuccess() {
    showSuccess(context, "Streams saved successfully");
  }

}