import 'dart:convert';
import 'dart:io';

import 'package:com/components/rounded_button.dart';
import 'package:com/models/account/user.dart';
import 'package:com/services/auth_service.dart';
import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  User user;
  TextEditingController nameController;
  TextEditingController phoneController;
  File _image;
  bool isBusy = false;
  TextEditingController groupController;
  List<String> groups = [
    "Student",
    "Parents",
    "Counselor",
    "Partner",
    "College staff"
  ];


  @override
  void initState() {
    user = User.get();
    nameController = TextEditingController(text: user.name);
    phoneController = TextEditingController(text: user.phone);
    groupController = TextEditingController(text: user.group);
    super.initState();
  }

  void doCameraUpload() async {
    Navigator.pop(context);
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    print(image);
    if(image != null){
      _image = image;
      setState(() {});
    }
  }

  void doGalleryUpload() async {
    Navigator.pop(context);
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    print(image);
    if(image != null){
      _image = image;
      setState(() {});
    }
  }

  void uploadAvatar(){
    showDialog(
      context: context,
      child: SimpleDialog(
        title: Text("Select Image Source"),
        children: <Widget>[
          SimpleDialogOption(
            child: Row(
              children: <Widget>[
                Icon(Icons.camera_alt, color: Theme.of(context).primaryColor),
                SizedBox(width: 10),
                Text("Camera")
              ],
            ),
            onPressed: doCameraUpload,
          ),
          SimpleDialogOption(
            child: Row(
              children: <Widget>[
                Icon(Icons.photo, color: Theme.of(context).primaryColor),
                SizedBox(width: 10),
                Text("Gallery")
              ],
            ),
            onPressed: doGalleryUpload,
          ),
        ],
      )
    );
  }

  submit(){
    setState(() {
      isBusy = true;
    });
    var data = {
      "name": user.name,
      'phone': user.phone,
      "group": user.group
    };
    if(_image != null){
      data['avatar'] = base64Encode(_image.readAsBytesSync());
    }
    AuthService().updateProfile(data).then((value){
      user.avatar = value['avatar'];
      user.save();
      showSuccess(context, value['message']);
    }).catchError((e){
      if(e.runtimeType == String){
        showError(context, e);
      }else{
        showError(context, e['message']);
      }
    }).whenComplete((){
      setState(() {
        isBusy = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ])),
        child: SafeArea(
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
              Widget>[
            IconButton(
                icon: Icon(Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                    color: Colors.white),
                onPressed: ()=> Navigator.pop(context)
            ),
            Expanded(
                child: ListView(
                    children: <Widget>[
                      Align(
                          alignment: Alignment.center,
                          child: SizedBox(
                              width: 180,
                              height: 180,
                              child: Stack(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      width: 170,
                                      height: 170,
                                      margin: EdgeInsets.only(right: 15),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(85)),
                                          border: Border.all(color: Colors.white),
                                          image: DecorationImage(
                                            fit: BoxFit.fill,
                                            image: _image != null ? FileImage(_image) : (isEmpty(user.avatar) ? AssetImage('assets/images/user_avatar.png') : NetworkImage(user.avatar))
                                          )
                                      ),
                                    ),
                                  ),
                                  Align(
                                      alignment: Alignment.bottomRight,
                                      child: Container(
                                        width: 50,
                                        height: 50,
                                        margin: EdgeInsets.only(right: 15),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(Radius.circular(25)),
                                            border: Border.all(
                                                color: Theme.of(context).primaryColor)),
                                        child: IconButton(
                                          padding: EdgeInsets.zero,
                                          icon: Icon(Icons.camera_alt,
                                            color: Theme.of(context).primaryColor.withOpacity(1)),
                                          onPressed: uploadAvatar
                                        ),
                                      )
                                  )
                                ],
                              )
                          )
                      ),
                      SizedBox(height: 50),
                      TextFormField(
                        controller: nameController,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "Name",
                          labelText: "Name",
                          labelStyle: TextStyle(color: Colors.white),
                          hintStyle: TextStyle(color: Colors.white70),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70, width: 1)
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70, width: 1)
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white, width: 1)
                          )
                        ),
                        onChanged: (value){
                          user.name = value;
                          setState(() {});
                        },
                      ),
                      SizedBox(height: 30),
                      TextFormField(
                        controller: phoneController,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "xxxxxxxxxx",
                          labelText: "Phone",
                          prefix: Text("+91"),
                          labelStyle: TextStyle(color: Colors.white),
                          hintStyle: TextStyle(color: Colors.white70),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70, width: 1)
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70, width: 1)
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white, width: 1)
                          )
                        ),
                        validator: (value){
                            if(value.isEmpty){
                              return "this field is required";
                            }
                            if(value.trim().length != 10){
                              return "enter a valid phone number";
                            }
                            return null;
                          },
                        onChanged: (value){
                          user.phone = value;
                          setState(() {});
                        },
                      ),
                      SizedBox(height: 30),
                      TextFormField(
                        readOnly: true,
                        style: TextStyle(color: Colors.white),
                        controller: groupController,
                        validator: (value){
                          if(value.isEmpty){
                            return "this field is required";
                          }
                          return null;
                        },
                        onTap: (){
                          showAppBottomSheetList<String>(
                            context: context,
                            items: groups,
                            itemBuilder: (String item) {
                              return ListTile(
                                title: Text(
                                  item,
                                ),
                              );
                            },
                            onItemSelected: (item) {
                              user.group = item;
                              groupController.text = item;
                              setState(() {});
                            },
                            title: "Tell us who you are",
                            hasSearch: true,
                            searchMatcher: (String item, String text) {
                              return item
                                  .toLowerCase()
                                  .contains(text.toLowerCase());
                            },
                          );
                        },
                        decoration: InputDecoration(
                          hintText: "Tell us who you are",
                          labelText: "Tell us who you are",
                          suffixIcon: Icon(Icons.keyboard_arrow_down, color: Colors.white),
                          labelStyle: TextStyle(color: Colors.white),
                          hintStyle: TextStyle(color: Colors.white70),
                          border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70, width: 1)
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70, width: 1)
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white, width: 1)
                          )
                        )
                      ),
                      SizedBox(height: 50),
                    ]
                )
            ),
            RoundedButton(
              label: "Save",
              labelColor: Theme.of(context).primaryColor,
              backgroundColor: Colors.white,
              isLoading: isBusy,
              disabled: isEmpty(user.name) || isEmpty(user.phone),
              onTap: submit,
            )
          ])
        ),
      )
    );
  }
}
