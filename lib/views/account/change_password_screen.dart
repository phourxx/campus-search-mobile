import 'package:com/components/app_scaffold.dart';
import 'package:com/components/rounded_button.dart';
import 'package:com/services/auth_service.dart';
import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';

class ChangePasswordScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _ChangePasswordScreenState();

}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {

  bool showPassword = false;
  bool showPassword2 = false;
  String password, newPassword;
  bool isBusy = false;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      scaffoldKey: scaffoldKey,
      showAppBar: true,
      title: "Change Password",
      centerTitle: true,
      builder: (c){
        return Container(
          color: Colors.white,
          child: SafeArea(
            child: ListView(
              padding: EdgeInsets.all(15),
              children: <Widget>[
                TextFormField(
                  obscureText: !showPassword,
                  decoration: InputDecoration(
                      hintText: "Password",
                      labelText: "Password",
                      suffix: IconButton(
                          padding: EdgeInsets.zero,
                          onPressed: ()=> setState((){
                            showPassword = !showPassword;
                          }),
                          icon: Icon(!showPassword ? Icons.visibility_off : Icons.visibility, color: Theme.of(context).primaryColor)
                      )
                  ),
                  validator: (value){
                    if(value.isEmpty){
                      return "this field is required";
                    }
                    if(value.length < 6 || value.length > 12){
                      return "password should be 6 to 12 characters";
                    }
                    return null;
                  },
                  onChanged: (value)=> setState(()=> password=value)
                ),
                SizedBox(height: 20),
                TextFormField(
                  obscureText: !showPassword2,
                  decoration: InputDecoration(
                      hintText: "New Password",
                      labelText: "New Password",
                      suffix: IconButton(
                          padding: EdgeInsets.zero,
                          onPressed: ()=> setState((){
                            showPassword2 = !showPassword2;
                          }),
                          icon: Icon(!showPassword2 ? Icons.visibility_off : Icons.visibility, color: Theme.of(context).primaryColor)
                      )
                  ),
                  onChanged: (value)=> setState(()=> newPassword=value)
                ),
                SizedBox(height: 20),
                RoundedButton(
                  label: "Change",
                  isLoading: isBusy,
                  disabled: isEmpty(password) || isEmpty(newPassword),
                  onTap: (){
                    setState((){isBusy=true;});
                    AuthService().changePassword({"password": password, "new_password": newPassword}).then((value){
                      showSuccess(context, value['message']);
                    }).catchError((e){
                      showError(context, e['message']);
                    }).whenComplete(() => setState((){isBusy = false;}));
                    // showSnackBar(scaffoldKey, context, "Password changed successfully");
                  },
                )
              ]
            )
          ),
        );
      },
    );
  }

}