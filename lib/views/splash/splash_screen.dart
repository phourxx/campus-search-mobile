import 'dart:convert';

import 'package:com/models/account/user.dart';
import 'package:com/utils/constants.dart';
import 'package:com/views/dashboard/dashboard_screen.dart';
import 'package:com/views/dashboard/partner_dashboard_screen.dart';
import 'package:com/views/onboarding/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatelessWidget {
  SplashScreen({
    Key key,
  }) : super(key: key);

  checkSession(context) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Future.delayed(Duration(seconds: 1), (){
      if(sharedPreferences.containsKey(USER_DATA_PREF)){
        User user = User.fromJson(jsonDecode(sharedPreferences.getString(USER_DATA_PREF)));
        user.save();
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (c)=> user.isPartner ? PartnerDashboardScreen() : DashboardScreen()
          ),
          (r)=> r == null
        );
      }else{
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=> LoginScreen()), (r)=> r == null);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    checkSession(context);
    return Material(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/splash.png"),
            fit: BoxFit.fill
          )
        ),
        alignment: Alignment.bottomCenter,
        child: SafeArea(child: Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Text("v1.0.0", style: TextStyle(color: Colors.white.withOpacity(.5), fontSize: 12))
        )),
      )
    );
  }
}