import 'package:com/components/app_scaffold.dart';
import 'package:com/components/rounded_button.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AddCourseScreen extends StatefulWidget {
  Course course;
  List<Course> courses;

  AddCourseScreen({this.course, @required this.courses});

  @override
  State<StatefulWidget> createState() => _AddCourseScreenState();

}

class _AddCourseScreenState extends State<AddCourseScreen> {
  Course course;
  TextEditingController typeController;
  TextEditingController courseCtrl;


  @override
  void initState() {
    course = widget.course ?? Course();
    typeController = TextEditingController(text: course.type?.text);
    courseCtrl = TextEditingController(text: course.title);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      showAppBar: true,
      title: "Add Course",
      centerTitle: true,
      builder: (c){
        return Container(
          color: Colors.white,
          child: SafeArea(
              child: ListView(
                  padding: EdgeInsets.all(15),
                  children: <Widget>[
                    TextFormField(
                      readOnly: true,
                      controller: courseCtrl,
                      onTap: (){
                        showAppBottomSheetList<Course>(
                          context: context,
                          items: widget.courses,
                          itemBuilder: (Course item) {
                            return ListTile(
                              title: Text(
                                item.title,
                              ),
                            );
                          },
                          onItemSelected: (item) {
                            course.id = item.id;
                            course.title = item.title;
                            courseCtrl.text = item.title;
                            setState(() {});
                          },
                          title: "Select Course",
                          hasSearch: true,
                          searchMatcher: (Course item, String text) {
                            return item.title
                              .toLowerCase()
                              .contains(text.toLowerCase());
                          },
                        );
                      },
                      decoration: InputDecoration(
                        hintText: "Course",
                        labelText: "Course",
                      )
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      initialValue: course.duration,
                      decoration: InputDecoration(
                        hintText: "Course duration",
                        labelText: "Course duration"
                      ),
                      validator: (value){
                        if(value.isEmpty){
                          return "this field is required";
                        }
                        return null;
                      },
                      onChanged: (value)=> setState(()=> course.duration=value)
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      initialValue: course.amount,
                      decoration: InputDecoration(
                        hintText: "Course price",
                        labelText: "Course price"
                      ),
                      keyboardType: TextInputType.numberWithOptions(decimal: true),
                      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                      validator: (value){
                        if(value.isEmpty){
                          return "this field is required";
                        }
                        return null;
                      },
                      onChanged: (value)=> setState(()=> course.amount=value)
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      readOnly: true,
                      controller: typeController,
                      onTap: (){
                        showAppBottomSheetList<CourseTypes>(
                          context: context,
                          items: CourseTypes.values,
                          itemBuilder: (CourseTypes item) {
                            return ListTile(
                              title: Text(
                                item.text,
                              ),
                            );
                          },
                          onItemSelected: (item) {
                            course.type = item;
                            typeController.text = item.text;
                            setState(() {});
                          },
                          title: "Select Course Type",
                          hasSearch: true,
                          searchMatcher: (CourseTypes item, String text) {
                            return item.text
                              .toLowerCase()
                              .contains(text.toLowerCase());
                          },
                        );
                      },
                      decoration: InputDecoration(
                        hintText: "Course type",
                        labelText: "Course type",
                      )
                    ),
                    SizedBox(height: 30),
                    RoundedButton(
                      label: "Save",
                      disabled: isEmpty(course.duration) || isEmpty(course.title) || isEmpty(course.amount) || course.type == null,
                      onTap: (){
                        Navigator.pop(context, course);
                      },
                    ),
                    SizedBox(height: 20)
                  ]
              )
          ),
        );
      },
    );
  }

}