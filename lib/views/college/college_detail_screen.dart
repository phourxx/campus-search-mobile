import 'package:com/components/app_scaffold.dart';
import 'package:com/components/custom_tab_indicator.dart';
import 'package:com/components/image_progress.dart';
import 'package:com/models/account/dashboard.dart';
import 'package:com/models/account/user.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/services/college_service.dart';
import 'package:com/views/college/tabs/about_tab.dart';
import 'package:com/views/college/tabs/courses_and_fees_tab.dart';
import 'package:com/views/college/tabs/gallery_tab.dart';
import 'package:com/views/college/tabs/news_tab.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:share_it/share_it.dart';

class CollegeDetailScreen extends StatefulWidget {

  College college;

  CollegeDetailScreen(this.college);

  @override
  State<StatefulWidget> createState() => _CollegeDetailScreenState();

}

//ValueNotifier<double> headerNotifier = ValueNotifier<double>(1);
BehaviorSubject<double> _headerNotifier = BehaviorSubject.seeded(1);
Stream<double> get headerNotifierStream => _headerNotifier.stream;

double get headerNotifier => _headerNotifier.value;

set headerNotifier(double value) {
  if (!_headerNotifier.isClosed) _headerNotifier.add(value);
}

class _CollegeDetailScreenState extends State<CollegeDetailScreen> {

  updateBookmark(){
    widget.college.bookmarked = !widget.college.bookmarked;
    setState((){});
    if(widget.college.bookmarked){
      CollegeService().bookmark(widget.college).then((value){
        Dashboard dashboard = Dashboard.persistence();
        dashboard.bookmarkedColleges.add(widget.college);
        dashboard.save();
      });
    }else{
      CollegeService().unbookmark(widget.college).then((value){
        Dashboard dashboard = Dashboard.persistence();
        dashboard.bookmarkedColleges.removeWhere((e)=> e.id == widget.college.id);
        dashboard.save();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      backgroundColor: Colors.white,
      builder: (c)=> DefaultTabController(
        length: 4,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverPersistentHeader(
                delegate: MySliverAppBar(
                  college: widget.college,
                  expandedHeight: 200,
                  context: context,
                  updateBookmark: updateBookmark
                ),
                pinned: true,
              ),
              SliverPersistentHeader(
                delegate: _SliverAppBarDelegate(
                  TabBar(
                    isScrollable: true,
                    indicatorSize: TabBarIndicatorSize.label,
                    labelStyle: TextStyle(fontSize: 19, fontWeight: FontWeight.bold, color: Colors.black),
                    unselectedLabelStyle: TextStyle(fontSize: 17, fontWeight: FontWeight.normal, color: Color(0xFFAAAAAA)),
                    labelColor: Colors.black,
                    indicator: CustomTabIndicator(color: Theme.of(context).primaryColor, margin: 15),
                    unselectedLabelColor: Color(0xFFAAAAAA),
                    tabs: [
                      Tab(text: "About"),
                      Tab(text: "Course & Fees"),
                      Tab(text: "News"),
                      Tab(text: "Gallery"),
                    ],
                  ),
                ),
                pinned: true,
              ),
            ];
          },
          body: StreamBuilder<double>(
            initialData: 1,
            stream: headerNotifierStream,
            builder: (c, s){
              return Container(
                  margin: EdgeInsets.only(top: (1-s.data) * 120 ),
                  child: TabBarView(
                      children: [
                        AboutTab(widget.college),
                        CoursesAndFeesTab(widget.college),
                        NewsTab(widget.college),
                        GalleryTab(widget.college)
                      ]
                  )
              );
            }
          ),
        ),
      ),
    );
  }

}
class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final College college;
  BuildContext context;
  Function updateBookmark;

  MySliverAppBar({@required this.college, @required this.expandedHeight, this.context, this.updateBookmark});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    headerNotifier = (1 - shrinkOffset / expandedHeight);
    print("headerNotifier $headerNotifier");
    return Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: [
          Image.network(
            college.image,
            fit: BoxFit.cover,
          ),
          Positioned.fill(
            child: Opacity(
              opacity: shrinkOffset / expandedHeight,
              child: Container(
                color: Colors.white,
                child: AppBar(
                  leading: IconButton(
                    icon: Icon(Icons.chevron_left),
                    onPressed: ()=> Navigator.pop(context)
                  ),
                  title: Text(college.name),
                  centerTitle: false,
                  actions: <Widget>[
                    IconButton(icon: Icon(Icons.share, color: Colors.white), onPressed: (){
                      ShareIt.text(
                        content: '${college.about}',
                        androidSheetTitle: 'Share ${college.name}'
                      );
                    }),
                    User.get().isPartner ? SizedBox.shrink() : IconButton(
                      icon: Icon(
                        college.bookmarked ? Icons.bookmark : Icons.bookmark_border,
                        color: Colors.white,
                      ),
                      onPressed: updateBookmark
                    )
                  ],
                ),
              ),
            ),
          ),
          headerNotifier > 0 ? Align(
            alignment: Alignment.bottomLeft,
            child: Opacity(
              opacity: headerNotifier,
              child: Container(
                color: Colors.black.withOpacity(0.3),
                padding: EdgeInsets.symmetric(vertical: headerNotifier * 20, horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(
                            TargetPlatform.iOS == Theme.of(context).platform
                                ? Icons.arrow_back_ios : Icons.arrow_back,
                            color: Colors.white),
                          onPressed: ()=> Navigator.pop(context)
                        ),
                        Spacer(),
                        IconButton(icon: Icon(Icons.share, color: Colors.white), onPressed: (){
                          ShareIt.text(
                            content: '${college.about}',
                            androidSheetTitle: 'Share ${college.name}'
                          );
                        }),
                        User.get().isPartner ? SizedBox.shrink() :
                        IconButton(
                          icon: Icon(
                            college.bookmarked ? Icons.bookmark : Icons.bookmark_border,
                            color: Colors.white,
                          ),
                          onPressed: updateBookmark
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.start,
                    ),
                    Spacer(),
                    Row(
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(right: 10),
                            width: 50,
                            height: headerNotifier * 50,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(25))
                            ),
                            child: Image.network(
                              college.logo,
                              fit: BoxFit.cover,
                              width: 40,
                              height: headerNotifier * 40,
                              loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                                if (loadingProgress == null) return child;
                                return Center(
                                  child: SizedBox(
                                      width: 35,
                                      height: 35,
                                      child: ImageProgress(loadingProgress)
                                  ),
                                );
                              },
                            )
                        ),
                        Expanded(child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(college.name,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: headerNotifier * 17.0,
                                fontWeight: FontWeight.w500
                              )
                            ),
                            Text("${college.city?.name}",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: headerNotifier * 10.0,
                                )
                            ),
                          ],
                        ))
                      ],
                    ),
                  ],
                )
              )
            ),
          ) : SizedBox.shrink(),
        ],
      );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent {
    int add = 20;
    if(Theme.of(context).platform == TargetPlatform.iOS){
      add = 70;
    }
    return kToolbarHeight + add;
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}