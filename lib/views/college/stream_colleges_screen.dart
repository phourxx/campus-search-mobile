import 'package:com/components/app_scaffold.dart';
import 'package:com/components/colleges_list.dart';
import 'package:com/controllers/college/college_controller.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';

class StreamCollegesScreen extends StatefulWidget {
  StreamObject stream;
  Course course;
  City city;

  StreamCollegesScreen(this.stream, [this.course, this.city]);

  @override
  State<StatefulWidget> createState() => _StreamCollegesState();

}

class _StreamCollegesState extends State<StreamCollegesScreen> {
  CollegeController controller;
  TextEditingController courseController;
  CourseTypes currentCourseType = CourseTypes.ug;
  Course get selectedCourse => widget.course;

  @override
  void initState() {
    controller = CollegeController();
    controller.loadStreamColleges(widget.stream, widget.city, selectedCourse);
    courseController = TextEditingController(text: selectedCourse?.title);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      showAppBar: true,
      title: "${widget.city.name} - ${widget.stream.name}",
      centerTitle: true,
      state: AppState(
        hasSearch: true,
        onSearchChanged: (query){
          controller.filterByCourse(selectedCourse, query: query);
        },
        onSearchExit: (){
          controller.filterByCourse(selectedCourse);
        }
      ),
      elevation: 0,
      builder: (_)=> StreamBuilder<PageState>(
        initialData: PageState.noData,
        stream: controller.pageStateStream,
        builder: (c, snapshot){
          return Column(
            children: <Widget>[
              // Container(
              //   color: Colors.white.withOpacity(.1),
              //   padding: EdgeInsets.symmetric(horizontal: 10),
              //   margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
              //   child: Row(children: <Widget>[
              //     Icon(
              //       Icons.school,
              //       color: Colors.white,
              //     ),
              //     SizedBox(width: 10),
              //     Expanded(
              //       child: TextField(
              //         controller: courseController,
              //         style: TextStyle(color: Colors.white),
              //         readOnly: true,
              //         decoration: InputDecoration(
              //           fillColor: Colors.transparent,
              //           focusColor: Colors.transparent,
              //           border: InputBorder.none,
              //           hintText: "Filter by course",
              //           hintStyle: TextStyle(color: Colors.white.withOpacity(.5)),
              //         ),
              //         onTap: widget.stream.courses.isEmpty ? null : (){
              //           showAppBottomSheetList<Course>(
              //             context: context,
              //             items: widget.stream.courses,
              //             itemBuilder: (Course item) {
              //               return ListTile(
              //                 title: Text(
              //                   item.title,
              //                 ),
              //               );
              //             },
              //             onItemSelected: (item) {
              //               courseController.text = item.title;
              //               widget.stream.courses.forEach((c){
              //                 c.selected = c == item;
              //               });
              //               setState(() {});
              //               controller.filterByCourse(item);
              //             },
              //             title: "Select course",
              //             hasSearch: true,
              //             searchMatcher: (Course item, String text) {
              //               return item.title
              //                   .toLowerCase()
              //                   .contains(text.toLowerCase());
              //             },
              //           );
              //         },
              //       )
              //     ),
              //     SizedBox(width: 10),
              //     Icon(
              //       Icons.keyboard_arrow_down,
              //       color: Colors.white,
              //     ),
              //   ]),
              // ),

              // Container(
              //   color: Colors.white,
              //   padding: EdgeInsets.all(10),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //     children: CourseTypes.values.map((t){
              //       return InkWell(
              //         onTap: currentCourseType == t ? null : (){
              //           currentCourseType = t;
              //           controller.load();
              //         },
              //         child: Column(
              //           children: <Widget>[
              //             Text(
              //                 t.text,
              //                 style: currentCourseType == t
              //                     ? TextStyle(
              //                     color: Colors.black,
              //                     fontSize: 15,
              //                     fontWeight: FontWeight.w500
              //                 )
              //                     : TextStyle(
              //                     color: Colors.grey,
              //                     fontSize: 13
              //                 )
              //             ),
              //             currentCourseType.value == t.value
              //                 ? Container(
              //                 height: 2, width: 20, margin: EdgeInsets.symmetric(horizontal: 15, vertical: 3), color: Theme.of(context).primaryColor)
              //                 : SizedBox.shrink()
              //           ],
              //         ),
              //       );
              //     }).toList(),
              //   )
              // ),
              Expanded(child: CollegeList(
                  pagesState: snapshot.data,
                  colleges: controller.colleges,
                  onRetry: ()=> controller.loadStreamColleges(widget.stream, widget.city, selectedCourse),
              ))
            ]
          );
        }
      )
    );
  }

}