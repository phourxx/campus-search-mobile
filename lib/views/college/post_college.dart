import 'package:com/components/app_scaffold.dart';
import 'package:com/components/course_list_item.dart';
import 'package:com/components/rounded_button.dart';
import 'package:com/controllers/college/post_college_controller.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/constants.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/college/add_course_screen.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:image_picker/image_picker.dart';

class PostCollegeScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _PostCollegeScreenState();

}

class _PostCollegeScreenState extends State<PostCollegeScreen> implements PostCollegeView {

  PostCollegeController controller;
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  bool isPredicting = false;


  @override
  void initState() {
    controller = PostCollegeController(this);
    controller.load();
    super.initState();
  }

  Widget _buildBasicDetail(){
    return Container(
      padding: EdgeInsets.all(15),
      child: ExpandablePanel(
        headerAlignment: ExpandablePanelHeaderAlignment.center,
        header: Container(
//          margin: EdgeInsets.only(top: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              controller.basicDetailValid
                  ? Icon(Icons.check_circle, color: Colors.green)
                  : Icon(
                  Icons.add_circle_outline,
                  color: Theme.of(context).primaryColor
              ),
              SizedBox(width: 10),
              Text("Basic Details", style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.black))
            ],
          )
        ),
        collapsed: SizedBox.shrink(),
        expanded: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    labelText: "College Name",
                    hintText: "College Name"
                ),
                onChanged: (value){
                  controller.name = value;
                },
              ),
              SizedBox(height: 10),
              Row(
                  children: <Widget>[
                    Expanded(child: TextField(
                      controller: addressController,
                      decoration: InputDecoration(
                        labelText: "College Address",
                        hintText: "College Address",
                      ),
                      onChanged: (value){
                        print("College address changed");
                        controller.address = value;
                      },
                      readOnly: true,
                      onTap: () async {
                        if(!isPredicting){
                          Prediction prediction = await PlacesAutocomplete.show(
                              context: context,
                              apiKey: kGoogleApiKey,
                              mode: Mode.overlay, // Mode.fullscreen
                              language: "en",
                              components: [
                                new Component(Component.country, "in"),
                              ]
                          );
                          if(prediction != null){
                            setState(() {
                              isPredicting = true;
                            });
                            PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(prediction.placeId);
                            controller.latitude = detail.result.geometry.location.lat;
                            controller.longitude = detail.result.geometry.location.lng;
                            addressController.text = detail.result.formattedAddress;
                            controller.address = detail.result.formattedAddress;
                            setState(() {
                              isPredicting = false;
                            });
                          }
                        }
                      },
                    )),
                    SizedBox(width: 10),
                    !isPredicting ? SizedBox.shrink() : SizedBox(
                      child: CircularProgressIndicator(),
                      width: 15,
                      height: 15,
                    ),
                    SizedBox(width: 10)
                  ]
              ),
              SizedBox(height: 10),
              TextField(
                controller: cityController,
                decoration: InputDecoration(
                    labelText: "College City",
                    hintText: "College City"
                ),
                readOnly: true,
                onTap: () {
                  showAppBottomSheetList<City>(
                    context: context,
                    items: controller.cities,
                    itemBuilder: (City item) {
                      return ListTile(
                        title: Text(
                          item.name,
                        ),
                      );
                    },
                    onItemSelected: (item) {
                      cityController.text = item.name;
                      controller.city = item;
                    },
                    title: "Select city",
                    hasSearch: true,
                    searchMatcher: (City item, String text) {
                      return item.name
                          .toLowerCase()
                          .contains(text.toLowerCase());
                    },
                  );
                },
              ),
              SizedBox(height: 10),
              TextField(
                decoration: InputDecoration(
                  labelText: "Description",
                  hintText: "Description"
                ),
                maxLines: 5,
                minLines: 1,
                keyboardType: TextInputType.multiline,
                onChanged: (value){
                  controller.description = value;
                  controller.description = value;
                },
              ),
            ]
        ),
        tapHeaderToExpand: true,
        hasIcon: true,
      )
    );
  }

  Widget _buildContactDetail(){
    return Container(
        padding: EdgeInsets.all(15),
        child: ExpandablePanel(
          header: Container(
//          margin: EdgeInsets.only(top: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  controller.contactDetailValid
                      ? Icon(Icons.check_circle, color: Colors.green)
                      : Icon(
                      Icons.add_circle_outline,
                      color: Theme.of(context).primaryColor
                  ),
                  SizedBox(width: 10),
                  Text("Contact Details", style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.black))
                ],
              )
          ),
          collapsed: SizedBox.shrink(),
          expanded: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(
                      labelText: "College Phone Number",
                      hintText: "College Phone Number"
                  ),
                  onChanged: (value){
                    controller.phone = value;
                  },
                ),
                TextField(
                  decoration: InputDecoration(
                      labelText: "College Email",
                      hintText: "College Email"
                  ),
                  onChanged: (value){
                    controller.email = value;
                  },
                ),
                TextField(
                  decoration: InputDecoration(
                      labelText: "College Website",
                      hintText: "College Website"
                  ),
                  onChanged: (value){
                    controller.website = value;
                  },
                ),
              ]
          ),
          tapHeaderToExpand: true,
          hasIcon: true,
        )
    );
  }

  Widget __buildCoursesAndFeesForm(){
    List<Widget> children = [];
    if(controller.college?.courses?.isEmpty ?? true){
      children.add(
        Align(
          alignment: Alignment.center,
          child: Text("No courses added yet."),
        )
      );
    }else{
      children.add(
        Align(
          child: Text("Swipe left to remove a course or tap to edit", textAlign: TextAlign.center),
          alignment: Alignment.center,
        )
      );
    }
    children.add(SizedBox(height: 20));
    if(controller.college?.courses != null){
      children.addAll(controller.college?.courses?.map((c){
        return Container(
            child: Dismissible(
              direction: DismissDirection.endToStart,
              key: Key(c.key),
              child: InkWell(
                onTap: () async{
                  Course course = await Navigator.push(context, MaterialPageRoute(builder: (con)=> AddCourseScreen(courses: controller.courses, course: c)));
                  if(course != null){
                    var college = controller.college;
  //                  int index = college.courses.indexOf(c);
  //                  college.courses.replaceRange(index, index+1, [course]);
                    controller.college = college;
                  }
                },
                child: CourseListItem(c)
              ),
              onDismissed: (direction){
                var college = controller.college;
                college.courses.remove(c);
                controller.college = college;
              },
              background: Container(color: Colors.redAccent),
            )
        );
      }));
    }
    children.add(
      Align(
        alignment: Alignment.center,
        child: OutlineButton(
          onPressed: () async{
            Course course = await Navigator.push(context, MaterialPageRoute(builder: (c)=> AddCourseScreen(courses: controller.courses)));
            if(course != null){
              var college = controller.college;
              college.courses.add(course);
              controller.college = college;
            }
          },
          child: Text("Add course"),
        )
      )
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: children,
    );
  }

  Widget _buildCoursesAndFeesForm(){
    return Container(
        padding: EdgeInsets.all(15),
        child: ExpandablePanel(
          header: Container(
//          margin: EdgeInsets.only(top: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  controller.college?.courses?.isNotEmpty ?? false
                      ? Icon(Icons.check_circle, color: Colors.green)
                      : Icon(
                      Icons.add_circle_outline,
                      color: Theme.of(context).primaryColor
                  ),
                  SizedBox(width: 10),
                  Text("Courses & fees", style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.black))
                ],
              )
          ),
          collapsed: SizedBox.shrink(),
          expanded: __buildCoursesAndFeesForm(),
          tapHeaderToExpand: true,
          hasIcon: true,
        )
    );
  }

  void doImageUpload(bool isLogo) async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    print(image);
    if(image != null){
      if(isLogo) controller.logo = image;
      else controller.image = image;
    }
  }

  Widget _buildPhotosForm(){
    return Container(
        padding: EdgeInsets.all(15),
        child: ExpandablePanel(
          header: Container(
//          margin: EdgeInsets.only(top: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  controller.photosValid
                      ? Icon(Icons.check_circle, color: Colors.green)
                      : Icon(
                      Icons.add_circle_outline,
                      color: Theme.of(context).primaryColor
                  ),
                  SizedBox(width: 10),
                  Text("Photos", style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.black))
                ],
              )
          ),
          collapsed: SizedBox.shrink(),
          expanded: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Align(
                  child: OutlineButton(
                      padding: controller.logo != null ? EdgeInsets.zero : null,
                      onPressed: (){
                        doImageUpload(true);
                      },
                      child: controller.logo != null
                          ? Image.file(
                        controller.logo,
                        fit: BoxFit.cover,
                        width: 80,
                        height: 80,
                      )
                          : Text("Click to upload college's logo")
                  ),
                  alignment: Alignment.center,
                ),
                SizedBox(height: 10),
                Align(
                  child: OutlineButton(
                    padding: controller.image != null ? EdgeInsets.zero : null,
                    onPressed: (){
                      doImageUpload(false);
                    },
                    child: controller.image != null
                        ? ConstrainedBox(
                            constraints: BoxConstraints(
                              maxHeight: 200
                            ),
                            child: Image.file(
                              controller.image,
                              width: double.infinity,
                              fit: BoxFit.cover,
                            ),
                          )
                        : Text("Click to upload college's image")
                  ),
                  alignment: Alignment.center,
                )
              ]
          ),
          tapHeaderToExpand: true,
          hasIcon: true,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PageState>(
      initialData: PageState.loading,
      stream: controller.pageStream,
      builder: (c, s){
        return AppScaffold(
          showAppBar: true,
          title: "Post College",
          state: AppState(
            pageState: s.data,
            onRetry: controller.load
          ),
          loadingBuilder: (c){
            return Container(
              alignment: Alignment.center,
              color: Colors.white,
              child: SizedBox(
                width: 30,
                height: 30,
                child: CircularProgressIndicator(),
              ),
            );
          },
          builder: (c){
            return Container(
              color: Colors.white,
              child: SafeArea(child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      children: <Widget>[
                        _buildBasicDetail(),
                        Divider(color: Color(0xFF555555)),
                        _buildContactDetail(),
                        Divider(color: Color(0xFF555555)),
                        _buildCoursesAndFeesForm(),
                        Divider(color: Color(0xFF555555)),
                        _buildPhotosForm(),
                        SizedBox(height: 15)
                      ]
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: RoundedButton(
                      label: "Submit",
                      disabled: !controller.canSubmit,
                      isLoading: controller.isSaving,
                      onTap: (){
                        controller.submit();
                      },
                    ),
                  ),
                  SizedBox(height: 15)
                ],
              ))
            );
          },
        );
      }
    );
  }

  @override
  onError() {
    showError(context, controller.error);
  }

  @override
  onSuccess(String msg) {
    showSuccess(context, msg);
  }

}