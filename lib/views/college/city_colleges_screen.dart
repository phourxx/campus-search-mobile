import 'package:com/components/app_scaffold.dart';
import 'package:com/components/colleges_list.dart';
import 'package:com/controllers/college/college_controller.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/utils/app_state.dart';
import 'package:flutter/material.dart';

class CityCollegesScreen extends StatefulWidget {
  City city;

  CityCollegesScreen(this.city);

  @override
  State<StatefulWidget> createState() => _StreamCollegesState();

}

class _StreamCollegesState extends State<CityCollegesScreen> {
  CollegeController controller;


  @override
  void initState() {
    // if(widget.city.)
    controller = CollegeController();
    controller.loadCityColleges(widget.city);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      showAppBar: true,
      title: "Colleges in ${widget.city.name}",
      centerTitle: true,
      state: AppState(
        hasSearch: true,
        onSearchChanged: (String query){
          controller.filterResults(Filter(), query: query);
        },
        onSearchExit: controller.clearFilter,
        onRetry: (){
          controller.loadCityColleges(widget.city);
        }
      ),
      builder: (_)=> StreamBuilder<PageState>(
        initialData: PageState.noData,
        stream: controller.pageStateStream,
        builder: (c, snapshot){
          return CollegeList(
            pagesState: snapshot.data,
            colleges: controller.colleges,
            onRetry: ()=> controller.loadCityColleges(widget.city),
          );
        }
      )
    );
  }

}