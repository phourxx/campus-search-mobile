import 'package:com/components/app_scaffold.dart';
import 'package:com/components/colleges_list.dart';
import 'package:com/models/account/dashboard.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/utils/app_state.dart';
import 'package:flutter/material.dart';

class ShortlistedCollegesScreen extends StatefulWidget {

  ShortlistedCollegesScreen();

  @override
  State<StatefulWidget> createState() => _ShortlistedCollegesState();
}

class _ShortlistedCollegesState extends State<ShortlistedCollegesScreen> {
  List<College> colleges = Dashboard.persistence()?.bookmarkedColleges ?? [];

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      showAppBar: true,
      state: AppState(
        hasSearch: true,
        onSearchChanged: (query) {
          setState(() {
            colleges = Dashboard.persistence()?.bookmarkedColleges?.where((element) => element.name.toLowerCase().contains(query.toLowerCase()))?.toList() ?? [];
          });
        },
        onSearchExit: () {
          colleges = Dashboard.persistence()?.bookmarkedColleges ?? [];
        }
      ),
      title: "Shortlisted Colleges",
      centerTitle: true,
      builder: (_) => CollegeList(
        pagesState: colleges.isEmpty ? PageState.noData : PageState.loaded, colleges: colleges, onRetry: (){},
      )
    );
  }
}
