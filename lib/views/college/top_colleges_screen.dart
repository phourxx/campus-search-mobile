import 'package:com/components/app_scaffold.dart';
import 'package:com/components/colleges_list.dart';
import 'package:com/controllers/college/college_controller.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/utils/app_state.dart';
import 'package:flutter/material.dart';

class TopCollegesScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _TopCollegesState();

}

class _TopCollegesState extends State<TopCollegesScreen> {
  CollegeController controller;


  @override
  void initState() {
    controller = CollegeController();
    controller.load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      showAppBar: true,
      title: "Top Colleges",
      centerTitle: true,
      state: AppState(
        hasSearch: true,
        pageState: PageState.loaded,
        onRetry: controller.load,
        onSearchChanged: (query){
          controller.filterResults(Filter(), query: query);
        },
        onSearchExit: controller.clearFilter,
      ),
      builder: (_)=> StreamBuilder<PageState>(
        initialData: PageState.noData,
        stream: controller.pageStateStream,
        builder: (c, snapshot){
          return CollegeList(
            pagesState: snapshot.data,
            colleges: controller.colleges,
            onRetry: controller.load,
          );
        }
      )
    );
  }

}