import 'package:com/components/course_list_item.dart';
import 'package:com/models/stream/college.dart';
import 'package:flutter/material.dart';

class CoursesAndFeesTab extends StatelessWidget {
  College college;

  CoursesAndFeesTab(this.college);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        
        children: college.courses.map((course){
          return CourseListItem(course);
        }).toList()
      )
    );
  }

}