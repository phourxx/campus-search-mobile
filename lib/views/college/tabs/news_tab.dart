import 'package:com/components/outline_container.dart';
import 'package:com/components/read_more_text.dart';
import 'package:com/models/stream/college.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

class NewsTab extends StatelessWidget {
  College college;

  NewsTab(this.college);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: college.news.isNotEmpty ? college.news.map((n){
          return OutlineContainer(
            padding: EdgeInsets.all(15),
            margin: EdgeInsets.only(top: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ReadMoreText(
                  n.content,
                  trimLines: 4,
                  style: TextStyle(fontSize: 14, height: 1.5),
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Text(
                    timeago.format(n.dateTime),
                    style: TextStyle(fontSize: 9),
                  ),
                )
              ]
            ),
          );
        }).toList() : [SizedBox(height: 30), Text("No news item")],
      ),
    );
  }

}