import 'package:com/components/image_progress.dart';
import 'package:com/models/stream/college.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class GalleryTab extends StatelessWidget {

  College college;

  GalleryTab(this.college);

  @override
  Widget build(BuildContext context) {
    return college.images.isNotEmpty ? StaggeredGridView.countBuilder(
      crossAxisCount: 2,
      itemCount: college.images.length,
      itemBuilder: (BuildContext context, int index) => InkWell(
        onTap: (){
          showDialog(
            context: context,
            child: SimpleDialog(
              contentPadding: EdgeInsets.zero,
              children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    child: Image.network(
                      college.images[index].imageUrl,
                      fit: BoxFit.cover,
                      loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                        if (loadingProgress == null) return child;
                        return Center(
                          child: SizedBox(
                              width: 35,
                              height: 35,
                              child: ImageProgress(loadingProgress)
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                    child: Text(college.images[index].caption, textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.white))
                  )
              ],
              backgroundColor: Colors.black.withOpacity(.3),
            )
          );
        },
        child: Image.network(
          college.images[index].imageUrl,
          fit: BoxFit.cover,
          loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
            if (loadingProgress == null) return child;
            return Center(
              child: SizedBox(
                  width: 50,
                  height: 50,
                  child: ImageProgress(loadingProgress)
              ),
            );
          },
        )
      ),
      staggeredTileBuilder: (int index) => new StaggeredTile.count(1, index.isEven ? 2 : 1),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    ) : Column(children: <Widget>[SizedBox(height: 30), Text("No item in gallery yet")],);
  }

}