import 'package:com/components/college_stream_tag.dart';
import 'package:com/components/read_more_text.dart';
import 'package:com/models/stream/college.dart';
import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';

class AboutTab extends StatelessWidget {

  College college;

  AboutTab(this.college);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(15),
            child: ReadMoreText(
              "${college.about}",
              style: TextStyle(fontSize: 14, height: 2),
              trimLength: 150,
            )
          ),
          SizedBox(
            height: 30,
            child: ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: 15),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (c, i)=> CollegeStreamTag(college.streams[i]),
              separatorBuilder: (c, i)=> SizedBox(width: 10),
              itemCount: college.streams.length
            )
          ),
          Divider(color: Color(0xFFE2E2E2), thickness: 3, height: 50),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(width: 15),
              Icon(Icons.location_on, color: Theme.of(context).primaryColor),
              SizedBox(width: 15),
              Expanded(
                child: Text("${college.address}", style: TextStyle(fontSize: 15, height: 2))
              ),
              SizedBox(width: 15),
              FlatButton(
                padding: EdgeInsets.zero,
                onPressed: (){
                  MapsLauncher.launchCoordinates(college.latitude, college.longitude);
                },
                child: Image.asset(
                  "assets/images/google_map_icon.png",
                  width: 40,
                  height: 40,
                  fit: BoxFit.contain
                )
              ),
              SizedBox(height: 15),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: <Widget>[
              SizedBox(width: 15),
              Icon(Icons.phone, color: Theme.of(context).primaryColor),
              SizedBox(width: 15),
              Expanded(
                child: Text("${college.phone}", style: TextStyle(fontSize: 15))
              ),
              SizedBox(width: 15),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: <Widget>[
              SizedBox(width: 15),
              Icon(Icons.email, color: Theme.of(context).primaryColor),
              SizedBox(width: 15),
              Expanded(
                child: Text("${college.email}", style: TextStyle(fontSize: 15))
              ),
              SizedBox(width: 15),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: <Widget>[
              SizedBox(width: 15),
              Icon(Icons.language, color: Theme.of(context).primaryColor),
              SizedBox(width: 15),
              Expanded(
                child: Text("${college.website}", style: TextStyle(fontSize: 15))
              ),
              SizedBox(width: 15),
            ],
          ),
        ],
      )
    );
  }

}