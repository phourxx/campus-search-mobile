import 'package:com/components/app_scaffold.dart';
import 'package:com/controllers/filter_controller.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';

class FilterScreen extends StatefulWidget {
  Filter filter;

  FilterScreen(this.filter);

  @override
  State<StatefulWidget> createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  FilterController controller;
  Filter _filter;
  TextEditingController cityController;
  bool initialized = false;

  @override
  void initState() {
    controller = FilterController();
    _filter = Filter.clone(widget.filter);
    controller.init(_filter);
    cityController = TextEditingController();
    super.initState();
  }

  void preSelect(state) {
    if (!initialized && state == PageState.loaded) {
      initialized = true;
      cityController.text = controller.cities
          .firstWhere((c) => c.id == _filter.cityId, orElse: () => null)
          ?.name;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          Navigator.pop(context, _filter);
          return new Future(() => false);
        },
        child: StreamBuilder<PageState>(
            initialData: PageState.loading,
            stream: controller.loadingStream,
            builder: (c, s) {
              preSelect(s.data);
              return AppScaffold(
                  showAppBar: true,
                  title: "Filter",
                  elevation: 0,
                  actions: <Widget>[
                    FlatButton(
                        textColor: Colors.white,
                        padding: EdgeInsets.zero,
                        onPressed: () {
                          _filter = widget.filter;
                          controller.init(_filter);
                        },
                        child: Text("Clear All"))
                  ],
                  state: AppState(
                    pageState: s.data,
                  ),
                  loadingBuilder: (c) {
                    return Container(
                      color: Colors.white,
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: 25,
                        height: 25,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  },
                  builder: (c) {
                    return Column(children: <Widget>[
                      Container(
                        color: Colors.black.withOpacity(.1),
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        margin:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Row(children: <Widget>[
                          Icon(
                            Icons.location_on,
                            color: Colors.white.withOpacity(.5),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                              child: TextField(
                                  style: TextStyle(color: Colors.white),
                                  controller: cityController,
                                  readOnly: true,
                                  decoration: InputDecoration(
                                    fillColor: Colors.transparent,
                                    focusColor: Colors.transparent,
                                    border: InputBorder.none,
                                    hintText: "Filter by city",
                                    hintStyle: TextStyle(
                                        color: Colors.white.withOpacity(.5)),
                                  ),
                                  onTap: () {
                                    showAppBottomSheetList<City>(
                                      context: context,
                                      items: controller.cities,
                                      itemBuilder: (City item) {
                                        return ListTile(
                                          title: Text(
                                            item.name,
                                          ),
                                        );
                                      },
                                      onItemSelected: (item) {
                                        _filter.cityId = item.id;
                                        cityController.text = item.name;
                                      },
                                      title: "Select city",
                                      hasSearch: true,
                                      searchMatcher: (City item, String text) {
                                        return item.name
                                            .toLowerCase()
                                            .contains(text.toLowerCase());
                                      },
                                    );
                                  }))
                        ]),
                      ),
                      Expanded(
                          child: Row(
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width * .4,
                              height: MediaQuery.of(context).size.height,
                              color: Color(0xfff9fafe),
                              child: ListView.builder(
                                  itemBuilder: (c, i) => InkWell(
                                      onTap: () {
                                        var streams = controller.streams;
                                        streams[streams
                                                .indexWhere((s) => s.selected)]
                                            .selected = false;
                                        streams[i].selected = true;
                                        controller.streams = streams;
                                      },
                                      child: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  controller.streams[i].selected
                                                      ? Colors.white
                                                      : null,
                                              border: controller
                                                      .streams[i].selected
                                                  ? Border(
                                                      left: BorderSide(
                                                          color: Colors.black
                                                              .withOpacity(
                                                                  0.1)),
                                                      bottom: BorderSide(
                                                          color: Colors.black
                                                              .withOpacity(
                                                                  0.1)),
                                                      top: BorderSide(
                                                          color: Colors.black
                                                              .withOpacity(
                                                                  0.1)),
                                                    )
                                                  : null),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 15),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Flexible(
                                                  child: Text(
                                                controller.streams[i].name,
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: Colors.black),
                                              )),
                                              Container(
                                                width: 20,
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                alignment: Alignment.center,
                                                height: 20,
                                                decoration: BoxDecoration(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                child: Text(
                                                    "${controller.streams[i].courses.where((c) => c.selected).length}",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 11)),
                                              )
                                            ],
                                          ))),
                                  itemCount: controller.streams.length)),
                          Expanded(
                              child: Container(
                            height: MediaQuery.of(context).size.height,
                            color: Colors.white,
                            child: ListView.builder(
                              itemBuilder: (s, i) {
                                return InkWell(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 15),
                                    decoration: BoxDecoration(
                                        border: Border(
                                      left: BorderSide(
                                          color: Colors.black.withOpacity(0.1)),
                                      bottom: BorderSide(
                                          color: Colors.black.withOpacity(0.1)),
                                    )),
                                    child: Row(
                                      children: <Widget>[
                                        controller.visibleCourses[i].selected
                                            ? Icon(Icons.check,
                                                color: Colors.green, size: 14)
                                            : SizedBox.shrink(),
                                        SizedBox(width: 10),
                                        Expanded(
                                            child: Text(
                                                controller
                                                    .visibleCourses[i].title,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black))),
                                        // SizedBox(width: 10),
                                        // Text("20",
                                        //     style: TextStyle(
                                        //         fontSize: 12,
                                        //         color: Colors.grey))
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    var streams = controller.streams;
                                    var c = streams[streams
                                            .indexWhere((s) => s.selected)]
                                        .courses[i];
                                    if (c.selected) {
                                      _filter.coursesId.remove(c.id);
                                    } else {
                                      _filter.coursesId.add(c.id);
                                    }
                                    streams[streams
                                            .indexWhere((s) => s.selected)]
                                        .courses[i]
                                        .selected = !c.selected;
                                    controller.streams = streams;
                                  },
                                );
                              },
                              itemCount: controller.visibleCourses.length,
                            ),
                          ))
                        ],
                      ))
                    ]);
                  });
            }));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
