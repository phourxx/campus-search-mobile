import 'package:com/components/college_stream_tag.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/components/image_progress.dart';
import 'package:com/views/college/college_detail_screen.dart';
import 'package:flutter/material.dart';

import 'outline_container.dart';

double kCollegeListItemMinHeight = 60;
class CollegeListItem extends StatelessWidget {

  final College college;
  final Function onTap;

  const CollegeListItem({this.college, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
          builder: (c)=> CollegeDetailScreen(college)
        ));
      },
      child: OutlineContainer(
          constraints: BoxConstraints(
            minHeight: kCollegeListItemMinHeight
          ),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                          college.name,
                          style: Theme.of(context).textTheme.title.copyWith(
                              fontSize: 14, fontWeight: FontWeight.w500
                          )
                      ),
                      SizedBox(height: 5),
                      Text(
                          "${college.city?.name}",
                          style: TextStyle(
                              fontSize: 11,
                              color: Colors.black.withOpacity(.8)
                          )
                      )
                    ]
                  )),
                  SizedBox(width: 15),
                  Column(
                    children: <Widget>[
                      college.isPremium ? Image.asset("assets/images/premium.png", width: 30, height: 30) : SizedBox.shrink(),
                      SizedBox(height: college.isPremium ? 10 : 0),
                      Image.network(
                        college.logo,
                        fit: BoxFit.contain,
                        width: 35,
                        height: 35,
                        loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: SizedBox(
                                width: 20,
                                height: 20,
                                child: ImageProgress(loadingProgress)
                            ),
                          );
                        },
                      )
                    ],
                  )
                ]
              ),
              SizedBox(height: 10),
              SizedBox(
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    // college.streams.isNotEmpty ? CollegeStreamTag(college.streams.first) : Container(),
                    // college.streams.length > 1 ? SizedBox(width: 10) : SizedBox.shrink(),
                    // college.streams.length > 1 ? Text("and more") : SizedBox.shrink(),
                    // Spacer(),
                    Expanded(
                      child: Container()
                    ),
                    SizedBox(width: 15),
                    Icon(
                      college.bookmarked ? Icons.bookmark : Icons.bookmark_border,
                      color: college.bookmarked ? Theme.of(context).primaryColor : Colors.black,
                    )
                  ]
                )
              )
            ]
          )
      )
    );
  }

}

