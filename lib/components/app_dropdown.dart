import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';

class AppDropdown<T> extends StatefulWidget {
  final String label;
  final bool enabled;
  final String hint;
  final T value;
  final List<T> items;
  final String Function(T item) displayItem;
  final Widget Function(T item) displayListTile;
  final ValueChanged<T> onSelect;
  final double displayItemTextSize;
  final bool centerValue;
  final bool hasSearch;

  /// If true and the list contains only one value, it will be selected
  /// and the dropdown is disabled
  final bool autoselect;

  /// Useful for approximating the total height of the scroll view
  final double itemHeight;

  const AppDropdown({
    Key key,
    @required this.items,
    @required this.displayItem,
    this.displayListTile,
    this.label,
    this.enabled = true,
    this.hint,
    this.value,
    this.onSelect,
    this.displayItemTextSize,
    this.centerValue = false,
    this.hasSearch = false,
    this.autoselect = true,
    this.itemHeight,
  }) : super(key: key);

  @override
  _AppDropdownState<T> createState() => _AppDropdownState<T>();
}

class _AppDropdownState<T> extends State<AppDropdown<T>> {
  bool enabled;

  void _chooseItem() async {
    showAppBottomSheetList<T>(
      context: context,
      items: widget.items,
      itemBuilder: (T item) {
        if (widget.displayListTile != null) return widget.displayListTile(item);
        return ListTile(
          title: Text(
            widget.displayItem(item),
          ),
        );
      },
      onItemSelected: widget.onSelect,
      title: widget.label ?? widget.hint,
      hasSearch: widget.hasSearch,
      searchMatcher: !widget.hasSearch
          ? null
          : (T item, String text) {
              return widget
                  .displayItem(item)
                  .toLowerCase()
                  .contains(text.toLowerCase());
            },
      itemHeight: widget.itemHeight,
    );
  }

  @override
  void initState() {
    enabled = widget.enabled;
    super.initState();
  }

  @override
  void didUpdateWidget(AppDropdown<T> oldWidget) {
    enabled = widget.enabled;
    checkForAutoSelect();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    checkForAutoSelect();
    super.didChangeDependencies();
  }

  void checkForAutoSelect() {
    if (widget.items?.isEmpty ?? true) {
      if (mounted)
        setState(() {
          enabled = false;
        });
    } else if (widget.items.length == 1 && widget.autoselect) {
      T firstAccount = widget.items.first;
      Future.delayed(Duration(milliseconds: 200)).then((_) {
        if (!mounted) return;
        setState(() {
          enabled = false;
        });
        widget.onSelect(firstAccount);
      });
    }
  }

  OutlineInputBorder get _border => OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: BorderSide(
          color: Color(0xFF555555),
          width: 0.5,
        ),
      );

  @override
  Widget build(BuildContext context) {
    // return AppInputDropdown(
    //   valueSize: widget.displayItemTextSize,
    //   showArrow: true,
    //   hintText: widget.hint,
    //   label: widget.label,
    //   valueText: widget.value != null ? widget.displayItem(widget.value) : null,
    //   onPressed: enabled ? _chooseItem : () => null,
    //   enabled: enabled && widget.items != null && widget.items.isNotEmpty,
    //   isValueCentered: widget.centerValue,
    //   showSuffix: false,
    // );
    String valueText =
        widget.value != null ? widget.displayItem(widget.value) : null;

    final Color primaryColor = Theme.of(context).textTheme.body1.color;

    final TextStyle labelStyle = Theme.of(context)
        .textTheme
        .body1;
    final TextStyle valueStyle =
        Theme.of(context).inputDecorationTheme.hintStyle.copyWith(
              color: primaryColor,
              fontSize: 16,
            );

    return InkWell(
      highlightColor: !enabled ? Colors.transparent : null,
      radius: !enabled ? 0 : null,
      onTap: enabled ? _chooseItem : () => null,
      child: InputDecorator(
        isFocused: true,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 12,
            vertical: 12,
          ),
          fillColor: enabled
              ? (Theme.of(context).brightness == Brightness.light
                  ? null
                  : Colors.transparent)
              : Theme.of(context).scaffoldBackgroundColor,
          hintText: widget.hint,
          hintStyle: valueStyle,
          enabled: enabled,
          border: _border,
          focusedBorder: _border,
          enabledBorder: _border,
          disabledBorder: _border,
        ),
        baseStyle: valueStyle,
        child: Row(
          mainAxisAlignment: widget.centerValue
              ? MainAxisAlignment.center
              : MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: valueText.isEmpty
                  ? Text(valueText, style: valueStyle)
                  : Text(widget.label, style: labelStyle),
            ),
            Icon(
              Icons.arrow_drop_down
            ),
          ],
        ),
      ),
    );
  }
}
