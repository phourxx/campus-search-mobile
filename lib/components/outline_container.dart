import 'package:flutter/material.dart';

class OutlineContainer extends StatelessWidget {

  Widget child;
  BoxConstraints constraints;
  EdgeInsetsGeometry padding;
  EdgeInsetsGeometry margin;
  Alignment alignment;
  Color color;

  OutlineContainer({this.child, this.color, this.constraints, this.padding, this.margin, this.alignment});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: constraints,
      padding: padding,
      margin: margin,
      alignment: alignment,
      decoration: BoxDecoration(
        color: color ?? Colors.white,
        border: Border.all(color: Color(0xFFDFDFDF)),
        borderRadius: BorderRadius.all(Radius.circular(5))
      ),
      child: child
    );
  }

}