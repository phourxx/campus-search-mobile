import 'package:flutter/material.dart';

class CustomTabIndicator extends Decoration {
  final BoxPainter _painter;

  CustomTabIndicator({@required Color color, @required double margin})
      : _painter = _CirclePainter(color, margin);

  @override
  BoxPainter createBoxPainter([onChanged]) => _painter;
}

class _CirclePainter extends BoxPainter {
  final Paint _paint;
  final double margin;

  _CirclePainter(Color color, this.margin)
      : _paint = Paint()
    ..color = color
    ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration cfg) {
    final Offset rectOffset =
        offset + Offset(cfg.size.width / 2, cfg.size.height - (margin/2));
    Rect rect = Rect.fromCenter(center: rectOffset, width: cfg.size.width - (margin*2), height: 2);
    RRect rRect = RRect.fromRectAndRadius(rect, Radius.circular(1));
    canvas.drawRRect(rRect, _paint);
  }
}