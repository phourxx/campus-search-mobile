import 'package:com/components/outline_container.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/components/image_progress.dart';
import 'package:flutter/material.dart';

double kStreamGridItemMinHeight = 90;
class StreamGridItem extends StatelessWidget {

  final StreamObject stream;
  final Function onTap;
  final double width;

  const StreamGridItem({this.stream, this.onTap, this.width});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: OutlineContainer(
          constraints: BoxConstraints(
            minWidth: width ?? 90,
            minHeight: kStreamGridItemMinHeight,
          ),
          color: stream.selected ? Theme.of(context).primaryColor : Colors.white,
          padding: EdgeInsets.all(5),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.network(
                stream.selected ? stream.selectedIcon : stream.icon,
                fit: BoxFit.contain,
                width: 30,
                height: 30,
                loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                    child: SizedBox(
                        width: 30,
                        height: 30,
                        child: ImageProgress(loadingProgress)
                    ),
                  );
                },
              ),
              SizedBox(height: 15),
              Text(
                stream.name,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 11,
                  color: stream.selected ? Colors.white : Colors.black
                )
              )
            ]
          )
      )
    );
  }

}

