import 'package:com/components/rounded_button.dart';
import 'package:com/utils/theme_data.dart';
import 'package:flutter/material.dart';

class ErrorWidget extends StatelessWidget {
  ErrorWidget({
    Key key,
    this.onRetry,
    @required this.message,
  });

  final VoidCallback onRetry;
  final String message;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      // color: AppTheme.backgroundColor,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(message),
            SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  child: RoundedButton(
                    onTap: onRetry,
                    label: "Retry",
                  ),
                  width: 150
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}