import 'package:com/models/stream/stream_object.dart';
import 'package:flutter/material.dart';

class CollegeStreamTag extends StatelessWidget {

  StreamObject stream;

  CollegeStreamTag(this.stream);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0xFFDFDFDF)),
            borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        // margin: EdgeInsets.symmetric(horizontal: 5),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(Icons.brightness_1, color: Color(0xFFE2E2E2), size: 8),
            SizedBox(width: 10),
            Text(
                stream.name,
                style: TextStyle(fontSize: 11, fontWeight: FontWeight.w500),
                overflow: TextOverflow.ellipsis
            )
          ],
        )
    );
  }

}