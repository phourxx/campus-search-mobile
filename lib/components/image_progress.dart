import 'package:flutter/material.dart';

class ImageProgress extends StatelessWidget{
  ImageChunkEvent loadingProgress;

  ImageProgress(this.loadingProgress);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator()
    );
    return CircularProgressIndicator(
      value: loadingProgress.expectedTotalBytes != null ?
      loadingProgress.cumulativeBytesLoaded /
          loadingProgress.expectedTotalBytes
          : null,
    );
  }

}