import 'dart:ui';

import 'package:com/components/error_widget.dart';
import 'package:com/components/search_bar.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/theme_data.dart';
import 'package:flutter/material.dart' hide ErrorWidget;

class AppScaffold extends StatefulWidget {
  AppScaffold({
    Key key,
    this.scaffoldKey,
    this.showAppBar,
    this.builder,
    this.loadingBuilder,
    this.title,
    this.actions,
    this.elevation,
    this.persistentFooterButtons,
    this.drawer,
    this.endDrawer,
    this.bottomNavigationBar,
    this.bottomSheet,
    this.backgroundColor,
    this.resizeToAvoidBottomInset = true,
    this.primary = true,
    this.state = const AppState(),
    this.disablePointer = false,
    this.centerTitle = true,
    this.textUnderLoader,
    this.extendBodyBehindAppBar = false,
  })  : assert(primary != null),
        super(key: key);

  final Key scaffoldKey;
  final bool showAppBar;
  final String title;
  final double elevation;
  final List<Widget> actions;
  final WidgetBuilder builder;
  final WidgetBuilder loadingBuilder;
  final List<Widget> persistentFooterButtons;
  final Widget drawer;
  final Widget endDrawer;
  final Color backgroundColor;
  final Widget bottomNavigationBar;
  final Widget bottomSheet;
  final bool resizeToAvoidBottomInset;
  final bool primary;
  final AppState state;
  final bool disablePointer;
  final bool centerTitle;
  final String textUnderLoader;
  final bool extendBodyBehindAppBar;

  static _AppScaffoldState of(BuildContext context, {bool nullOk = false}) {
    final _AppScaffoldState result =
        context.ancestorStateOfType(const TypeMatcher<_AppScaffoldState>());
    return result;
  }

  @override
  State<StatefulWidget> createState() => _AppScaffoldState();
}

class _AppScaffoldState extends State<AppScaffold> {
  bool _isSearching = false;

  bool get isSearching {
    return _isSearching;
  }

  List<Widget> _requestedActions;
  Widget _requestedAppbar;
  AppState _requestedState;

  bool _showBlur = false;

  set isSearching(bool value) {
    _isSearching = value;
    if (!value) {
      if (_requestedState?.onSearchChanged != null) {
        _requestedState.onSearchChanged(null);
      } else if (widget.state.onSearchChanged != null) {
        widget.state.onSearchChanged(null);
      }
    }
  }

  void requestAppBarRefresh(PreferredSizeWidget appbar,
      {AppState appState, bool forceAppbar = false}) {
    _requestedState = appState;
    bool hasSearch = appState?.hasSearch ?? false;
    bool hadAppbar = _requestedAppbar != null;
    bool requestingAppbar = appbar != null;
    if (hadAppbar != requestingAppbar) refreshState();
    if (forceAppbar)
      _requestedAppbar = appbar;
    else
      _requestedAppbar = null;

    if (hasSearch)
      _requestedActions = _addSearchToActions(widget.actions);
    else
      _requestedActions = List.from(widget.actions ?? []);
  }

  void refreshState() {
    var state = AppScaffold.of(context);
    if (state != null) {
      state.refreshState();
    } else {
      Future.delayed(Duration(milliseconds: 16)).then((_) {
        if (mounted)
          setState(() {
            _requestedAppbar = _requestedAppbar;
          });
        else
          refreshState();
      });
    }
  }

  @override
  void initState() {
    super.initState();
//    if (widget.floatingActionButton is MultiFloatingActionButton) {}
  }

  void _clearPressed() {
    _resetSearch();
  }

  Widget _buildAppBar(BuildContext context) {
    _AppScaffoldState state = AppScaffold.of(context);
    if (widget.showAppBar ?? false) {
      var appbar;
      if (_requestedAppbar != null) {
        appbar = _requestedAppbar;
      } else if (isSearching) {
        appbar = SearchBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => _stopSearching(),
          ),
          onClear: _clearPressed,
          onSubmit:
              _requestedState?.onSearchSubmit ?? widget.state?.onSearchSubmit,
          onSearchChanged:
              _requestedState?.onSearchChanged ?? widget.state?.onSearchChanged,
        );
      } else {
        print("Navbar");
        if(_requestedActions == null){
          bool hasSearch = widget.state?.hasSearch ?? false;
          if (hasSearch)
            _requestedActions = _addSearchToActions(widget.actions);
          else
            _requestedActions = List.from(widget.actions ?? []);
        }
        appbar = AppBar(
          key: widget.key,
          automaticallyImplyLeading: true,
          title: Text(widget.title),
          actions: _requestedActions,
          backgroundColor: widget.backgroundColor,
          primary: widget.primary,
          centerTitle: widget.centerTitle,
          elevation: widget.elevation,
        );
      }
      if (state != null) {
        print("STATE");
        state.requestAppBarRefresh(appbar, appState: widget.state, forceAppbar: false);
        return _EmptyAppBar();
      } else {
        return appbar;
      }
    }
    return null;
  }

  List<Widget> _addSearchToActions(List<Widget> appbarActions) {
    Widget searchButton = IconButton(
        padding: EdgeInsets.zero,
        icon: Icon(Icons.search, color: Colors.white),
        onPressed: () {
          setState(() {
            _resetSearch();
            isSearching = true;
          });
        });
    List<Widget> actions = appbarActions ?? [];
    return List.from(actions)..add(searchButton);
  }

  @override
  Widget build(BuildContext context) {
    Widget pageBody = SizedBox.shrink();
    switch (widget.state.pageState) {
      case PageState.loading:
        pageBody = Builder(
          builder: widget.loadingBuilder,
        );
        break;
      case PageState.loaded:
        if (widget.builder != null) {
          pageBody = Builder(
            builder: widget.builder,
          );
        }
        break;
      case PageState.error:
        pageBody = ErrorWidget(
          message: 'An error has occured',
          onRetry: widget.state.onRetry,
        );
        break;
      case PageState.noData:
        if (widget.state.noDataMessage != null) {
          pageBody = Center(
              child: Text(widget.state.noDataMessage,
                  style: TextStyle(color: Theme.of(context).primaryColor)));
        }
        break;
    }

    Widget scaffold = Scaffold(
      key: widget.scaffoldKey,
      appBar: _buildAppBar(context),
      body: AbsorbPointer(
        absorbing: widget.disablePointer,
        child: Stack(
          children: [
            SizedBox.expand(child: pageBody),
            _buildBlur(),
          ],
        ),
      ),
      persistentFooterButtons: widget.persistentFooterButtons,
      drawer: widget.drawer,
      endDrawer: widget.endDrawer,
      bottomNavigationBar: widget.bottomNavigationBar,
      bottomSheet: widget.bottomSheet,
      backgroundColor: widget.backgroundColor ?? AppTheme.backgroundColor,
      resizeToAvoidBottomInset: widget.resizeToAvoidBottomInset,
      primary: widget.primary,
      extendBodyBehindAppBar: widget.extendBodyBehindAppBar,
    );

    if (isSearching)
      return WillPopScope(
        onWillPop: _willPop,
        child: scaffold,
      );

    return scaffold;
  }

  void toggleBlur() {
    setState(() {
      _showBlur = !_showBlur;
    });
  }

  Widget _buildBlur() {
    return Visibility(
      visible: _showBlur,
      child: GestureDetector(
        // onTap: toggleBlur,
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.8),
            ),
          ),
        ),
      ),
    );
  }

  void _resetSearch() {
    if (_requestedState?.onSearchChanged != null) {
      _requestedState.onSearchChanged('');
    } else if (widget.state.onSearchChanged != null) {
      widget.state.onSearchChanged('');
    }
  }

  Future<bool> _willPop() async {
    if (isSearching) {
      _stopSearching();
      return false;
    }
    return true;
  }

  void _stopSearching() {
    setState(() {
      isSearching = false;
    });
    Function call = _requestedState?.onSearchExit ?? widget.state?.onSearchExit;
    call();
  }
}


class _EmptyAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  Size get preferredSize => Size(0, 0);
}
