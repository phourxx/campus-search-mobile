import 'package:com/models/stream/college.dart';
import 'package:com/components/image_progress.dart';
import 'package:com/views/college/college_detail_screen.dart';
import 'package:flutter/material.dart';

double kCollegeGridItemMinHeight = 150;
double kCollegeGridItemVerticalSpacing = 20;
class CollegeGridItem extends StatelessWidget {

  final College college;
  final Function onTap;
  final double width;

  const CollegeGridItem({this.college, this.onTap, this.width});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
            builder: (c)=> CollegeDetailScreen(college)
        ));
      },
      child: Container(
          constraints: BoxConstraints(
            minWidth: width ?? 160,
            minHeight: kCollegeGridItemMinHeight,
          ),
          padding: EdgeInsets.zero,
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: Image.network(
                  college.image,
                  width: double.infinity,
                  height: 120,
                  fit: BoxFit.fill,
                  loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                    if (loadingProgress == null) return child;
                    return Center(
                      child: SizedBox(
                          width: 50,
                          height: 50,
                          child: ImageProgress(loadingProgress)
                      ),
                    );
                  },
                )
              ),
              SizedBox(height: 15),
              Text(
                college.name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                  color: Colors.black
                ),
              )
            ]
          )
      )
    );
  }

}

