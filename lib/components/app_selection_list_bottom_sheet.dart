import 'package:com/utils/utils.dart';
import 'package:flutter/material.dart';

class AppSelectionListBottomSheet<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(T) itemBuilder;
  final ValueChanged<T> onItemSelected;
  final String title;
  final bool hasSearch;
  final bool Function(T, String) searchMatcher;
  final double itemHeight;
  final Future<List<T>> itemsFuture;

  const AppSelectionListBottomSheet({
    Key key,
    this.items,
    this.itemsFuture,
    this.itemBuilder,
    this.onItemSelected,
    this.title,
    this.itemHeight,
    this.hasSearch = false,
    this.searchMatcher,
  })  : assert(items != null || itemsFuture != null, "items cannot be null"),
        assert(itemBuilder != null, "itemBuilder cannot be null"),
        assert(onItemSelected != null, "onItemSelected cannot be null"),
        assert(title != null, "title cannot be null"),
        assert(
            hasSearch && searchMatcher != null ||
                !hasSearch && searchMatcher == null,
            "if search enabled, matcher cannot be null"),
        super(key: key);

  @override
  _AppSelectionListBottomSheetState<T> createState() =>
      _AppSelectionListBottomSheetState<T>();
}

class _AppSelectionListBottomSheetState<T>
    extends State<AppSelectionListBottomSheet<T>> {
  String _searchQuery;
  bool _keyboardOpened = false;

  List<T> _items;

  @override
  void initState() {
    super.initState();
    _items = widget.items ?? [];
  }

  List<T> get _filteredItems => (widget.hasSearch && isNotEmpty(_searchQuery))
      ? _items
          .where((item) => widget.searchMatcher(item, _searchQuery))
          .toList()
      : _items;

  Widget _buildToWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 21),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 60.0,
            height: 5.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(3.0),
              ),
              color: Theme.of(context).primaryColor,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitle() {
    return Padding(
      padding: const EdgeInsets.only(top: 22.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            widget.title,
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  Widget _buildSearchBox() {
    return widget.hasSearch
        ? Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: TextField(
              onTap: () => setState(() {
                _keyboardOpened = true;
              }),
              decoration: InputDecoration(
                fillColor: Colors.white,
                contentPadding: const EdgeInsets.only(top: 16),
                hintText: "Select an option",
                prefixIcon: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Icon(Icons.search),
                ),
              ),
              onChanged: (searchQuery) => setState(() {
                _searchQuery = searchQuery;
              }),
              onSubmitted: (text) {
                setState(() {
                  _keyboardOpened = false;
                });
              },
            ),
          )
        : SizedBox.shrink();
  }

  Widget _buildItemsList() {
    return Expanded(
      child: ListView.separated(
        itemBuilder: (context, index) => InkWell(
            onTap: () {
              widget.onItemSelected(_filteredItems[index]);
              Navigator.of(context).pop();
            },
            child: widget.itemBuilder(_filteredItems[index])),
        separatorBuilder: (context, index) => Divider(height: 1),
        itemCount: _filteredItems.length,
      ),
    );
  }

  double get _heightFactorFromHeight {
    if (widget.itemHeight == null) return 0.5;
    double screenHeight = MediaQuery.of(context).size.height;
    double maxRatio = 0.85;
    double minRatio = 0.1;
    double paddingRatio = 0.08;
    double bottomSheetHeight = widget.itemHeight * widget.items.length;
    bottomSheetHeight = bottomSheetHeight + (widget.hasSearch ? 100.0 : 50.0);
    double ratio = bottomSheetHeight / screenHeight;
    ratio += paddingRatio;
    if (ratio > maxRatio) return maxRatio;
    if (ratio < minRatio) return minRatio;
    return ratio;
  }

  Widget _buildFutureBody() {
    double height = MediaQuery.of(context).size.height * 0.5;
    return FutureBuilder<List<T>>(
      future: widget.itemsFuture,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Container(
            height: height,
            child: Center(
              child: Text("An error occurred"),
            ),
          );
        } else if (snapshot.hasData) {
          if (snapshot.data == null || snapshot.data.isEmpty) {
            return Container(
              height: height,
              child: Center(
                child: Text("No data"),
              ),
            );
          } else {
            _items = snapshot.data;
            return _buildItemsBody();
          }
        } else {
          return Container(
            height: height,
            child: Center(
              child: SizedBox(
                child: CircularProgressIndicator(),
                width: 25,
                height: 25,
              ),
            ),
          );
        }
      },
    );
  }

  Widget _buildItemsBody() {
    return FractionallySizedBox(
      heightFactor: _keyboardOpened ? 0.85 : _heightFactorFromHeight,
      child: Column(
        children: <Widget>[
          _buildToWidget(),
          _buildTitle(),
          _buildSearchBox(),
          _buildItemsList(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.itemsFuture != null ? _buildFutureBody() : _buildItemsBody();
  }
}
