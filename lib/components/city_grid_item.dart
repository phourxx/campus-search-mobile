import 'package:com/components/outline_container.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/components/image_progress.dart';
import 'package:com/views/college/city_colleges_screen.dart';
import 'package:flutter/material.dart';

double kCityGridItemMinHeight = 100;
double kCityGridItemVerticalSpacing = 20;
class CityGridItem extends StatelessWidget {

  final City city;
  final Function onTap;

  const CityGridItem({this.city, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap ?? (){
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (c)=> CityCollegesScreen(city)
          )
        );
      },
      child: OutlineContainer(
          constraints: BoxConstraints(
            minWidth: 100,
            minHeight: kCityGridItemMinHeight,
          ),
          padding: EdgeInsets.all(5),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.network(
                city.icon,
                fit: BoxFit.contain,
                width: 45,
                height: 45,
                loadingBuilder: (BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                    child: SizedBox(
                        width: 45,
                        height: 45,
                        child: ImageProgress(loadingProgress)
                    ),
                  );
                },
              ),
              SizedBox(height: 15),
              Text(
                city.name,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 11,
                  color: Colors.black
                )
              )
            ]
          )
      )
    );
  }

}

