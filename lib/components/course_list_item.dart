import 'package:com/components/outline_container.dart';
import 'package:com/models/account/user.dart';
import 'package:com/models/stream/college.dart';
import 'package:flutter/material.dart';

class CourseListItem extends StatelessWidget{
  Course course;
  User user = User.get();

  CourseListItem(this.course);

  @override
  Widget build(BuildContext context) {
    return OutlineContainer(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Row(
              children: <Widget>[
                Flexible(child: Text(
                    "${course.title}",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)
                )),
                Container(
                  margin: EdgeInsets.only(left: 5),
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 6),
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(20))
                  ),
                  child: Text(course.type.value, style: TextStyle(fontSize: 10, color: Colors.white)),
                )
              ]
          ),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Icon(Icons.access_time, color: Color(0xFF555555), size: 14),
              Text(
                  " ${course.duration}",
                  style: TextStyle(fontSize: 13, color: Color(0xFF555555), fontWeight: FontWeight.w500)
              ),
              Spacer(),
              Text("Fees: ", style: TextStyle(fontSize: 15, color: Color(0xFF555555), fontWeight: FontWeight.w500)),
              (user?.isPartner ?? false) ? Column(
                children: <Widget>[
                  Text("Rs.${course.amount}", style: TextStyle(fontSize: 16, color: Colors.grey, fontWeight: FontWeight.bold)),
                  SizedBox(height: 8),
                  Text("Rs.${course.partnerAmount}", style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold)),
                  // SizedBox(height: 8),
                  // Text("Rs.${course.amount}", style: TextStyle(fontSize: 12, color: Colors.grey, fontWeight: FontWeight.bold, decoration: TextDecoration.lineThrough)),
                ],
              ) : Text("Rs.${course.amount}", style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold)),
              Spacer()
            ]
          ),
        ],
      ),
    );
  }

}