import 'package:com/components/app_scaffold.dart';
import 'package:com/components/college_list_item.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/utils/app_state.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CollegeList extends StatelessWidget{

  PageState pagesState;
  List<College> colleges;
  Function onRetry;

  CollegeList({this.pagesState, this.colleges, @required this.onRetry});

  Widget _buildLoading(_){
    return SafeArea(bottom: false, left: false, right: false, child: Container(
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Shimmer.fromColors(
            baseColor: Colors.grey[300].withOpacity(1),
            highlightColor: Colors.grey[100].withOpacity(1),
            child: Container(
                color: Colors.black,
                height: 70
            ),
          ),
          SizedBox(height: 20),
          Shimmer.fromColors(
            baseColor: Colors.grey[300].withOpacity(1),
            highlightColor: Colors.grey[100].withOpacity(1),
            child: Container(
                color: Colors.black,
                height: 70
            ),
          ),
          SizedBox(height: 20),
          Shimmer.fromColors(
            baseColor: Colors.grey[300].withOpacity(1),
            highlightColor: Colors.grey[100].withOpacity(1),
            child: Container(
                color: Colors.black,
                height: 70
            ),
          ),
          SizedBox(height: 20),
          Shimmer.fromColors(
            baseColor: Colors.grey[300].withOpacity(1),
            highlightColor: Colors.grey[100].withOpacity(1),
            child: Container(
                color: Colors.black,
                height: 70
            ),
          )
        ],
      ),
    ));
  }

  Widget _buildContent(_){
    return ListView.separated(
        padding: EdgeInsets.all(15),
        itemCount: colleges.length,
        separatorBuilder: (c, i)=> SizedBox(height: 15),
        itemBuilder: (c, index){
          return CollegeListItem(
            college: colleges[index],
            onTap: (){

            },
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      backgroundColor: Colors.white,
      state: AppState(
          pageState: pagesState,
          noDataMessage: "No data to display",
          onRetry: onRetry
      ),
      loadingBuilder: _buildLoading,
      builder: _buildContent,
    );
  }

}