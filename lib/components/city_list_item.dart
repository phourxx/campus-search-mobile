import 'package:com/components/outline_container.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/views/college/city_colleges_screen.dart';
import 'package:flutter/material.dart';

class CityListItem extends StatelessWidget {

  City city;

  CityListItem(this.city);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
          builder: (c)=> CityCollegesScreen(city)
        ));
      },
      child: OutlineContainer(
        padding: EdgeInsets.all(15),
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(child: Text("${city.name}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black))),
              SizedBox(width: 15),
              Text("Colleges: ${city.collegeCount}", style: TextStyle(fontSize: 11, fontWeight: FontWeight.w500, color: Color(0xFF555555))),
              SizedBox(width: 10),
              Icon(Icons.chevron_right, color: Colors.black, size: 18)
            ]
        ),
      ),
    );
  }

}