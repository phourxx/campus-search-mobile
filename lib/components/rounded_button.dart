import 'package:flutter/material.dart';

class RoundedButton extends StatefulWidget {
  final bool disabled;
  final bool isLoading;
  final String label;
  final Function onTap;
  final Color backgroundColor;
  final Color labelColor;
  final double labelSize;

  const RoundedButton(
      {this.disabled = false, this.isLoading = false, this.label, this.onTap, this.backgroundColor, this.labelColor, this.labelSize});

  @override
  State<StatefulWidget> createState() => _RoundedButtonState();
}

class _RoundedButtonState extends State<RoundedButton> {
  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: widget.disabled ? 0.5 : 1,
      child: InkWell(
          onTap: widget.disabled ? null : widget.onTap,
          child: Container(
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: widget.backgroundColor ?? (widget.disabled
                      ? Color(0xFF707070)
                      : Theme.of(context).primaryColor),
                  gradient: widget.backgroundColor != null || widget.disabled
                      ? null
                      : LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      stops: [
                        0,
                        1
                      ],
                      colors: [
                        Theme.of(context).primaryColorDark,
                        Theme.of(context).primaryColor,
                      ]),
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              child: widget.isLoading
                  ? SizedBox(
                  width: 15, height: 15, child: CircularProgressIndicator())
                  : Text(
                widget.label,
                style: TextStyle(
                  fontSize: widget.labelSize ?? 18,
                  color: widget.labelColor ?? Colors.white,
                  letterSpacing: 0.36,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              )))
    );
  }
}
