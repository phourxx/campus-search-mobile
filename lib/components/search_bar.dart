import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget implements PreferredSizeWidget {
  SearchBar({
    Key key,
    this.onClear,
    this.onSearchChanged,
    this.onSubmit,
    this.leading,
    this.backgroundColor,
  }) : super(
          key: key,
        );
  final ValueChanged<String> onSearchChanged;
  final VoidCallback onClear;
  final VoidCallback onSubmit;
  final Widget leading;
  final Color backgroundColor;

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  SearchBarState createState() => SearchBarState();
}

class SearchBarState extends State<SearchBar> with TickerProviderStateMixin {
  TextEditingController _searchController;
  AnimationController _animationController;
  Animatable<Offset> _positionTween;
  Animation<Offset> _position;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
    _searchController.addListener(() => setState(() {}));
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
    _positionTween = Tween<Offset>(
      begin: const Offset(0.0, -0.3),
      end: Offset.zero,
    ).chain(CurveTween(
      curve: Curves.easeOut,
    ));
    _position = _positionTween.animate(_animationController);
    _animationController.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _animationController?.dispose();
    _searchController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: _position,
      child: AppBar(
        leading: widget.leading,
        title: TextField(
          autofocus: true,
          textInputAction: TextInputAction.search,
          controller: _searchController,
          onChanged: widget.onSearchChanged,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
            hintText: "Search",
            hintStyle: TextStyle(color: Colors.grey),
          ),
          onSubmitted: (value) => widget.onSubmit(),
        ),
        backgroundColor: widget.backgroundColor,
        actions: [
          Visibility(
            visible: _searchController.text.isNotEmpty,
            child: IconButton(
              tooltip: "Clear",
              icon: Icon(Icons.clear),
              onPressed: () {
                _searchController.text = '';
                widget.onClear();
              },
            ),
          )
        ],
      ),
    );
  }
}
