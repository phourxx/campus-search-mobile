import 'package:com/models/account/dashboard.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/services/stream_service.dart';
import 'package:com/utils/app_state.dart';
import 'package:rxdart/rxdart.dart';

class StreamController {

  SaveStreamsView view;
  StreamController([this.view]);
  StreamService service = StreamService();
  var error;
  BehaviorSubject<PageState> _pageState =
      BehaviorSubject.seeded(PageState.loading);
  BehaviorSubject<bool> _isSaving = BehaviorSubject.seeded(false);
  BehaviorSubject<List<StreamObject>> _streams = BehaviorSubject.seeded(null);

  Stream<PageState> get pageStateStream => _pageState.stream;

  PageState get pageState => _pageState.value;

  set pageState(PageState value) {
    if (!_pageState.isClosed) _pageState.add(value);
  }

  Stream<PageState> get loadingStream {
    return Rx.combineLatest3(
      pageStateStream,
      streamListStream,
      isSavingStream,
      (a, b, c) => pageState,
    );
  }

  Stream<bool> get isSavingStream => _isSaving.stream;

  bool get isSaving => _isSaving.value;

  set isSaving(bool value) {
    if (!_isSaving.isClosed) _isSaving.add(value);
  }

  bool get canSubmit {
    return streams.where((e) => e.selected).isNotEmpty;
  }

  Stream<bool> get canSubmitStream {
    List<Stream> streams = [
      streamListStream,
    ];
    return Rx.combineLatest(streams, (_) => canSubmit);
  }

  Stream<List<StreamObject>> get streamListStream => _streams.stream;

  List<StreamObject> get streams => _streams.value;

  set streams(List<StreamObject> value) {
    if (!_streams.isClosed) _streams.add(value);
  }


  loadStreams(FetchStreamFilter filter) {
    pageState = PageState.loading;
    service.fetchStreams(filter).then((streams) {
      Dashboard dashboard = Dashboard.persistence();
      if(filter == FetchStreamFilter.all){
        streams.forEach((element) {
          element.selected = dashboard.streams.firstWhere((element1) => element.id == element1.id, orElse: ()=> null) != null;
        });
      }
      this.streams = streams;
      if (streams.isEmpty) {
        pageState = PageState.noData;
      } else {
        pageState = PageState.loaded;
      }
    }).catchError((e) {
      error = e;
      pageState = PageState.error;
    });
  }

  saveStreams(){
    isSaving = true;
    service.saveStreams(streams.where((element) => element.selected).toList()).then((value){
      Dashboard dashboard = Dashboard.persistence();
      dashboard.streams = streams.where((element) => element.selected).toList();
      dashboard.streams.forEach((element) {element.selected = false;});
      dashboard.save();
      view.onSuccess();
    }).catchError((e) {
      error = e;
      view.onError();
    }).whenComplete(() => isSaving = false);
  }
}
abstract class SaveStreamsView {
  void onSuccess();
  void onError();
}
