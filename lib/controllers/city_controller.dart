import 'package:com/models/stream/city.dart';
import 'package:com/services/college_service.dart';
import 'package:com/utils/app_state.dart';
import 'package:rxdart/rxdart.dart';

class CityController {
  var error;
  BehaviorSubject<PageState> _pageState = BehaviorSubject.seeded(PageState.loading);
  BehaviorSubject<List<City>> _cities = BehaviorSubject.seeded([]);
  List<City> rawCities;

  Stream<PageState> get pageStateStream => _pageState.stream;

  PageState get pageState => _pageState.value;
  
  CollegeService service = CollegeService();

  set pageState(PageState value) {
    if (!_pageState.isClosed) _pageState.add(value);
  }

  Stream<PageState> get loadingStream {
    return Rx.combineLatest2(
      pageStateStream,
      cityListStream,
          (a, b) => pageState,
    );
  }

  Stream<List<City>> get cityListStream => _cities.stream;

  List<City> get cities => _cities.value;

  set cities(List<City> value) {
    if (!_cities.isClosed) _cities.add(value);
  }

  set streams(List<City> value) {
    if (!_cities.isClosed) _cities.add(value);
  }

  void load(){
    pageState = PageState.loading;
    // Future.delayed(Duration(seconds: 3), (){
    //   pageState = PageState.loaded;
    //   colleges = Dashboard().topColleges;
    // });
    // return;
    service.fetchCities().then((data){
      cities = data;
      rawCities = data;
      if(data.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }).catchError((e){
      error = e;
      pageState = PageState.error;
    });
  }

  filterCities(String query){
    pageState = PageState.loading;
    cities = rawCities.where((city) => city.name.toLowerCase().contains(query?.toLowerCase())).toList();
    if(cities.isEmpty){
      pageState = PageState.noData;
    }else{
      pageState = PageState.loaded;
    }
  }

  clearFilter(){
    pageState = PageState.loading;
    cities = rawCities;
    if(cities.isEmpty){
      pageState = PageState.noData;
    }else{
      pageState = PageState.loaded;
    }
  }
}