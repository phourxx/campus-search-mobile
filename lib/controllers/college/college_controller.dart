import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/services/college_service.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/utils.dart';
import 'package:com/views/dashboard/partner_dashboard_screen.dart';
import 'package:rxdart/rxdart.dart';

class CollegeController {
  PartnerDashboardView view;
  CollegeController([this.view]);
  CollegeService service = CollegeService();
  List<College> colleges;
  List<College> rawColleges;

  var error;
  BehaviorSubject<PageState> _pageState = BehaviorSubject.seeded(PageState.noData);
  Stream<PageState> get pageStateStream => _pageState.stream;

  PageState get pageState => _pageState.value;

  set pageState(PageState value) {
    if (!_pageState.isClosed) _pageState.add(value);
  }

  void load({bool trending=false}){
    pageState = PageState.loading;
    // Future.delayed(Duration(seconds: 3), (){
    //   pageState = PageState.loaded;
    //   colleges = Dashboard().topColleges;
    // });
    // return;
    service.list(trending: trending).then((data){
      this.colleges = data;
      this.rawColleges = data;
      if(data.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }).catchError((e, x){
      print("-------------");
      print(e);
      print(x);
      if(error.runtimeType == String){
        error = e;
      }else{
        error = e['message'];
      }
      if(error.toString().contains("Unauthenticated")){
        view?.onUnauthenticated();
      }
      pageState = PageState.error;
    });
  }

  bool applyFilter(College college, Filter filter){
    return filter.coursesId.isEmpty || college.courses.where(
      (course) => filter.coursesId.contains(course.courseId)
    ).isNotEmpty;
  }

  bool applyQuery(College college, String query){
    return college.name.toLowerCase().contains(query.toLowerCase())
      || college.courses.where((course) => course.title.toLowerCase().contains(query.toLowerCase())).isNotEmpty;
  }

  void filterResults(Filter filter, {String query}){
    if(rawColleges != null){
      pageState = PageState.loading;
      if(filter.cityId != null){
        colleges = rawColleges.where((college) => college.city.id == filter.cityId).where((college) => applyFilter(college, filter)).toList();
      }else{
        colleges = rawColleges.where((college) => applyFilter(college, filter)).toList();
      }
      if(isNotEmpty(query)){
        colleges = colleges.where((college) => applyQuery(college, query)).toList();
      }
      if(colleges.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }
  }

  void filterByCourse(Course course, {String query}){
    if(course != null){
      pageState = PageState.loading;
      if(isNotEmpty(query)){
        colleges = rawColleges.where((college) => (college.courses.where((c) => c.courseId == course.id).isNotEmpty) && college.name.toLowerCase().contains(query.toLowerCase())).toList();
      }else{
        colleges = rawColleges.where((college) => college.courses.where((c) => c.courseId == course.id).isNotEmpty).toList();
      }
      if(colleges.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }
  }

  void loadCityColleges(City city){
    pageState = PageState.loading;
    service.cityColleges(city: city).then((data){
      this.colleges = data;
      this.rawColleges = data;
      if(data.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }).catchError((e){
      error = e;
      pageState = PageState.error;
    });
  }

  void loadStreamColleges(StreamObject streamObject, City city, Course course){
    pageState = PageState.loading;
    service.streamColleges(stream: streamObject, city: city).then((data){
      this.colleges = data;
      this.rawColleges = data;
      if(data.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
      this.filterByCourse(course);
    }).catchError((e){
      error = e;
      pageState = PageState.error;
    });
  }

  void clearFilter(){
    pageState = PageState.loading;
    colleges = rawColleges;
    if(colleges.isEmpty){
      pageState = PageState.noData;
    }else{
      pageState = PageState.loaded;
    }
  }

}