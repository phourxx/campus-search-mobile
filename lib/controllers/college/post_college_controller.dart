import 'dart:convert';
import 'dart:io';

import 'package:com/models/account/dashboard.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/services/college_service.dart';
import 'package:com/services/stream_service.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/utils.dart';
import 'package:rxdart/rxdart.dart';

class PostCollegeController {
  PostCollegeView view;
  PostCollegeController(this.view);
  CollegeService service = CollegeService();
  List<College> colleges;
  var error;
  BehaviorSubject<PageState> _pageState = BehaviorSubject.seeded(PageState.loading);
  BehaviorSubject<bool> _isSaving = BehaviorSubject.seeded(false);
  BehaviorSubject<College> _college = BehaviorSubject.seeded(College());
  BehaviorSubject<List<City>> _cities = BehaviorSubject.seeded(null);
  BehaviorSubject<List<Course>> _courses = BehaviorSubject.seeded(null);
  BehaviorSubject<File> _logo = BehaviorSubject.seeded(null);
  BehaviorSubject<File> _image = BehaviorSubject.seeded(null);

  String get name => college.name;

  set name(String value){
    College c = college;
    c.name = value;
    college = c;
  }

  String get address => college.address;

  set address(String value){
    College c = college;
    c.address = value;
    college = c;
  }

  String get description => college.about;

  set description(String value){
    College c = college;
    c.about = value;
    college = c;
  }

  String get phone => college.phone;

  set phone(String value){
    College c = college;
    c.phone = value;
    college = c;
  }

  String get email => college.email;

  set email(String value){
    College c = college;
    c.email = value;
    college = c;
  }

  String get website => college.website;

  set website(String value){
    College c = college;
    c.website = value;
    college = c;
  }

  double get longitude => college.longitude;

  set longitude(double value){
    College c = college;
    c.longitude = value;
    college = c;
  }

  double get latitude => college.latitude;

  set latitude(double value){
    College c = college;
    c.latitude = value;
    college = c;
  }

  City get city => college.city;

  set city(City value){
    College c = college;
    c.city = value;
    college = c;
  }

  Stream<File> get logoStream => _logo.stream;

  File get logo => _logo.value;

  set logo(File value) {
    if(value != null){
      College c = college;
      c.logo = base64Encode(value.readAsBytesSync());
      college = c;
    }
    if (!_logo.isClosed) _logo.add(value);
  }

  Stream<File> get imageStream => _image.stream;

  File get image => _image.value;

  set image(File value) {
    if(value != null){
      College c = college;
      c.image = base64Encode(value.readAsBytesSync());
      college = c;
    }
    if (!_image.isClosed) _image.add(value);
  }

  Stream<College> get collegeStream => _college.stream;

  College get college => _college.value;

  set college(College value) {
    if (!_college.isClosed) _college.add(value);
  }

  Stream<PageState> get pageStateStream => _pageState.stream;

  PageState get pageState => _pageState.value;

  set pageState(PageState value) {
    if (!_pageState.isClosed) _pageState.add(value);
  }

  Stream<PageState> get pageStream {
    return Rx.combineLatest7(
      pageStateStream,
      collegeStream,
      cityListStream,
      streamListStream,
      isSavingStream,
      logoStream,
      imageStream,
          (a, b, c, d, e, f, g) => pageState,
    );
  }

  Stream<bool> get isSavingStream => _isSaving.stream;

  bool get isSaving => _isSaving.value;

  set isSaving(bool value) {
    if (!_isSaving.isClosed) _isSaving.add(value);
  }

  Stream<List<City>> get cityListStream => _cities.stream;

  List<City> get cities => _cities.value;

  set cities(List<City> value) {
    if (!_cities.isClosed) _cities.add(value);
  }

  Stream<List<Course>> get streamListStream => _courses.stream;

  List<Course> get courses => _courses.value;

  set courses(List<Course> value) {
    if (!_courses.isClosed) _courses.add(value);
  }

  Future<List<City>> loadCities() async{
    return service.fetchCities();
  }

  Future<List<Course>> loadCourses() async{
    return service.courses();
  }

  void load(){
    pageState = PageState.loading;
    Future.wait([
      loadCities(),
      loadCourses()
    ]).then((response){
      this.cities = response[0];
      this.courses = response[1];
      if(cities.isEmpty || courses.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }).catchError((e){
      error = e;
      pageState = PageState.error;
    });
  }

  bool get canSubmit {
    return basicDetailValid && contactDetailValid && photosValid && college.courses.isNotEmpty;
  }

  bool get basicDetailValid {
    return isNotEmpty(college.name) && college.city != null
        && isNotEmpty(college.address) && isNotEmpty(college.about);
  }

  bool get contactDetailValid {
    return isNotEmpty(college.phone) && isNotEmpty(college.email)
        && isNotEmpty(college.website);
  }

  bool get photosValid {
    return logo != null && image != null;
  }

  void submit(){
    isSaving = true;
    service.postCollege(college).then((value){
      college = College();
      logo = null;
      image = null;
      view.onSuccess(value['message']);
    }).catchError((e){
      this.error = e['message'];
      view.onError();
    }).whenComplete((){
      isSaving = false;
    });
  }


}

abstract class PostCollegeView {
  onSuccess(String msg);
  onError();
}