import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/filter.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/services/college_service.dart';
import 'package:com/services/stream_service.dart';
import 'package:com/services/urls.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/utils/cache.dart';
import 'package:rxdart/rxdart.dart';

class FilterController {
  StreamService streamService = StreamService();
  String error;

  BehaviorSubject<PageState> _pageState =
      BehaviorSubject.seeded(PageState.loading);
  BehaviorSubject<bool> _isSaving = BehaviorSubject.seeded(false);
  BehaviorSubject<List<StreamObject>> _streams = BehaviorSubject.seeded(null);
  BehaviorSubject<List<City>> _cities = BehaviorSubject.seeded(null);

  Stream<PageState> get pageStateStream => _pageState.stream;

  PageState get pageState => _pageState.value;

  set pageState(PageState value) {
    if (!_pageState.isClosed) _pageState.add(value);
  }

  Stream<PageState> get loadingStream {
    return Rx.combineLatest4(
      pageStateStream,
      streamListStream,
      cityListStream,
      isSavingStream,
      (a, b, c, d) => pageState,
    );
  }

  Stream<bool> get isSavingStream => _isSaving.stream;

  bool get isSaving => _isSaving.value;

  set isSaving(bool value) {
    if (!_isSaving.isClosed) _isSaving.add(value);
  }

  bool get canSubmit {
    return streams.where((e) => e.selected).isNotEmpty;
  }

  Stream<bool> get canSubmitStream {
    List<Stream> streams = [
      streamListStream,
    ];
    return Rx.combineLatest(streams, (_) => canSubmit);
  }

  Stream<List<StreamObject>> get streamListStream => _streams.stream;

  Stream<List<City>> get cityListStream => _cities.stream;

  List<StreamObject> get streams => _streams.value;

  List<City> get cities => _cities.value;

  StreamObject get selectedStream {
    int i = streams.indexWhere((s) => s.selected);
    if (i < 0) {
      return null;
    }
    return streams[i];
  }

  List<Course> get visibleCourses => selectedStream?.courses ?? [];

  set streams(List<StreamObject> value) {
    if (!_streams.isClosed) _streams.add(value);
  }

  set cities(List<City> value) {
    if (!_cities.isClosed) _cities.add(value);
  }

  List<StreamObject> __streams;

  void init(Filter filter) {
    Future.wait([loadCities(), loadStreams()]).then((response) {
      cities = response[0];
      Cache.set(Urls.cities, cities);
      __streams = response[1];
      Cache.set(Urls.streams, __streams);
      if(__streams.isNotEmpty){
        __streams.first.selected = true;
        if (filter.coursesId.isEmpty) {
          __streams.first.selected = true;
        }
        __streams.forEach((s) {
          s.courses.forEach((c) {
            c.selected = filter.coursesId.contains(c.id);
          });
        });
      }
      streams = __streams;
      if (streams.isEmpty || cities.isEmpty) {
        pageState = PageState.noData;
      } else {
        pageState = PageState.loaded;
      }
    }).catchError((e) {
      error = e.runtimeType == String ? e : e['message'];
      pageState = PageState.error;
    });
  }

  Future<List<City>> loadCities() async{
    List<City> cities = Cache.get(Urls.cities);
    if(cities != null){
      return Future.value(cities);
    }
    return CollegeService().fetchCities();
  }

  Future<List<StreamObject>> loadStreams() async{
    List<StreamObject> streams = Cache.get(Urls.streams);
    if(streams != null){
      return Future.value(streams);
    }
    return streamService.fetchStreams(FetchStreamFilter.all);
  }

  void dispose() {
    if (!_pageState.isClosed) _pageState.close();
    if (!_cities.isClosed) _cities.close();
    if (!_streams.isClosed) _streams.close();
    if (!_isSaving.isClosed) _isSaving.close();
  }
}
