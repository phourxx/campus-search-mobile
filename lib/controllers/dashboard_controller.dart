import 'package:com/models/account/dashboard.dart';
import 'package:com/models/account/notification.dart';
import 'package:com/services/dashboard_service.dart';
import 'package:com/utils/app_state.dart';
import 'package:com/views/dashboard/dashboard_screen.dart';
import 'package:flutter/rendering.dart';
import 'package:rxdart/rxdart.dart' hide Notification;

class DashboardController {
  DashboardView view;
  DashboardController(this.view);
  DashboardService service = DashboardService();
  Dashboard dashboard;
  List<Notification> notifications;
  List<ImageProvider> banners = [];

  var error;
  BehaviorSubject<PageState> _pageState = BehaviorSubject.seeded(PageState.loading);
  BehaviorSubject<int> _counter = BehaviorSubject.seeded(0);
  Stream<PageState> get pageStateStream => _pageState.stream;
  Stream<int> get bannerCounterStream => _counter.stream;

  PageState get pageState => _pageState.value;

  set pageState(PageState value) {
    if (!_pageState.isClosed) _pageState.add(value);
  }

  int get bannerCounter => _counter.value;

  set bannerCounter(int value) {
    if (!_counter.isClosed) _counter.add(value);
  }

  void startBannerCounter(){
    // if(bannerCounter)
    Future.delayed(Duration(seconds: dashboard.bannerDelay), (){
      if(bannerCounter == banners.length-1){
        bannerCounter = 0;
      }else{
        bannerCounter++;
      }
      startBannerCounter();
    });
  }

  void loadBanners(){
    for (var banner in dashboard.banners) {
      banners.add(NetworkImage(banner.image));
    }
  }

  void load(){
    // Future.delayed(Duration(seconds: 5), (){
    //   pageState = PageState.loaded;
    //   dashboard = Dashboard();
    // });
    // return;
    pageState = PageState.loading;
    service.fetch().then((data){
      this.dashboard = data;
      if(data.banners.isNotEmpty){
        loadBanners();
        startBannerCounter();
      }
      if(data.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }).catchError((e, x){
      print(e);
      print(x);
      if(error.runtimeType == String){
        error = e;
      }else{
        error = e['message'];
      }
      if(error.toString().contains("Unauthenticated")){
        view.onUnauthenticated();
      }
      pageState = PageState.error;
    });
  }

  void fetchNotifications(){
    service.fetchNotifications().then((data){
      this.notifications = data;
      if(data.isEmpty){
        pageState = PageState.noData;
      }else{
        pageState = PageState.loaded;
      }
    }).catchError((e){
      error = e;
      pageState = PageState.error;
    });
  }
}