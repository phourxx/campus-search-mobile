import 'dart:collection';

class Cache{
  static HashMap<dynamic, dynamic> _memory = HashMap();

  static dynamic get(url){
    print("CacheLogger:: fetching $url");
    if(_memory.containsKey(url)){
      print("CacheLogger:: found $url");
      return _memory[url];
    }
    print("CacheLogger:: not found $url");
    return null;
  }

  static void set(url, value){
    print("CacheLogger:: setting $url, $value");
    _memory[url] = value;
  }

  static void clear({String url}){
    print("CacheLogger:: clearing $url");
    if(url != null){
      _memory.remove(url);
    }else{
      Cache._memory = Map();
    }
  }
}