abstract class Enum {
  const Enum.internal(this.text, this.value);

  final String text;
  final String value;

  static const List<Enum> values = [];

}