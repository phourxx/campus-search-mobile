import 'package:flutter/material.dart';

class AppTheme{
  static final Color backgroundColor = Color(0xFF1E64A7);
  static final ThemeData themeData = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.blue,
    primaryColor: Color(0xFF1E64A7),
    primaryColorDark: Color(0xff0f4d72),
    fontFamily: "Nunito Sans",
    buttonTheme: ButtonThemeData(

    )
  );
}

