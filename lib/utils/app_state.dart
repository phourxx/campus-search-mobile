import 'package:flutter/material.dart';

enum PageState{
  loading,
  loaded,
  noData,
  error
}

class AppState {
  final PageState pageState;

  /// to show no data default widget, if null doesn't appear
  final String noDataMessage;

  /// to show search button, it's added at the end of the actions
  final bool hasSearch;
  final ValueChanged<String> onSearchChanged;

  /// when an error is showing, a retry button will be display
  final VoidCallback onRetry;

  /// when the keyboard done button for the search textfield is pressed
  final VoidCallback onSearchSubmit;

  /// when the keyboard done button for the search textfield is pressed
  final VoidCallback onSearchExit;

  // final String query;

  const AppState({
    this.pageState = PageState.loaded,
    this.noDataMessage = '',
    this.hasSearch = false,
    this.onSearchChanged,
    this.onSearchSubmit,
    this.onSearchExit,
    this.onRetry,
  });
}