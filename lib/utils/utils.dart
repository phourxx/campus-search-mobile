import 'dart:math';

import 'package:com/components/app_selection_list_bottom_sheet.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';


const ASCII_START = 33;
const ASCII_END = 126;
const LOWER_ALPHA_START = 97;
const LOWER_ALPHA_END = 122;
const UPPER_ALPHA_START = 65;
const UPPER_ALPHA_END = 90;


void showAppBottomSheetList<T>({
  @required BuildContext context,
  List<T> items,
  Future<List<T>> itemsFuture,
  @required Widget Function(T) itemBuilder,
  @required ValueChanged<T> onItemSelected,
  @required String title,
  double itemHeight,
  bool hasSearch = false,
  bool Function(T, String) searchMatcher,
}) {
  showModalBottomSheet(
    isDismissible: true,
    isScrollControlled: true,
    context: context,
    builder: (context) => AppSelectionListBottomSheet(
      title: title,
      items: items,
      itemsFuture: itemsFuture,
      itemBuilder: itemBuilder,
      onItemSelected: onItemSelected,
      itemHeight: itemHeight,
      hasSearch: hasSearch,
      searchMatcher: searchMatcher,
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
      ),
    ),
  );
}

/// Returns [true] if [s] is either null or empty.
bool isEmpty(String s) => s == null || s.isEmpty || s == 'null';

/// Returns [true] if [s] is a not null or empty string.
bool isNotEmpty(String s) => s != null && s.isNotEmpty && s != 'null';

void showSnackBar(GlobalKey<ScaffoldState> scaffoldKey, context, message){
  SnackBar snackbar = SnackBar(
      backgroundColor: Theme.of(context).primaryColorDark,
      content: Text(message, style: TextStyle(color: Colors.white, fontSize: 15))
  );
  scaffoldKey.currentState.showSnackBar(snackbar);
}

/// Generates a random integer where [from] <= [to].
int randomBetween(int from, int to) {
  if (from > to) throw Exception('$from cannot be > $to');
  var rand = Random();
  return from + rand.nextInt(to - from + 1);
}

/// Generates a random string of [length] with characters
/// between ascii [from] to [to].
/// Defaults to characters of ascii '!' to '~'.
String randomString(int length, {int from: ASCII_START, int to: ASCII_END}) {
  return new String.fromCharCodes(
      new List.generate(length, (index) => randomBetween(from, to)));
}

void showError(context, message){
  Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: "$message",
      buttons: [
        DialogButton(
          child: Text(
            "Close",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
}
void showSuccess(context, message){
  Alert(
      context: context,
      type: AlertType.success,
      title: "Success",
      desc: "$message",
      buttons: [
        DialogButton(
          child: Text(
            "Close",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
}
void showInfo(context, message){
  Alert(
      context: context,
      type: AlertType.info,
      title: "",
      desc: "$message",
      buttons: [
        DialogButton(
          child: Text(
            "Okay",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
}

List<City> extractCities(List<College> colleges){
  List<City> cities = [];
  for (var college in colleges) {
    if(cities.firstWhere((element) => element.id==college.city.id) == null){
      cities.add(college.city);
    }
  }
  return cities;
}

List<City> extractCourses(List<College> colleges){
  List<City> cities = [];
  for (var college in colleges) {
    if(cities.firstWhere((element) => element.id==college.city.id) == null){
      cities.add(college.city);
    }
  }
  return cities;
}