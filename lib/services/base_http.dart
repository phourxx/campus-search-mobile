import 'dart:convert';

import 'package:com/utils/cache.dart';
import 'package:http/http.dart' as http;

Future<dynamic> get(String url, {dynamic headers, bool cache=true}) async{
  try{
    print("RequestLogger:: GET $url, $headers");
    // var cached = Cache.get(url);
    // if(isNotEmpty(cached)){
    //   return Future.value(json.decode(cached));
    // }
    http.Response response =  await http.get(url, headers: headers).timeout(Duration(seconds: 60));
    print("ResponseLogger:: $url, ${response.statusCode}, ${response.body}");
    if(response.statusCode == 200){
      if(cache){
        Cache.set(url, response.body);
      }
      return Future.value(json.decode(response.body));
    }else if(response.statusCode == 400 || response.statusCode == 422 || response.statusCode == 401){
      return Future.error(json.decode(response.body));
    }else{
      return Future.error("Server error occurred");
    }
  }catch(e){
    print("----");
    print(e);
    print("----");
    return Future.error("Connection error, please try again.");
  }
}

Future<dynamic> post(String url, {dynamic data, dynamic headers}) async{
  try{
    print("RequestLogger:: POST $url, $data, $headers");
    http.Response response = await http.post(url, body: jsonEncode(data), headers: headers).timeout(Duration(seconds: 60));
    print("ResponseLogger:: $url, ${response.statusCode}, ${response.body}");
    if(response.statusCode == 200){
      Cache.set(url, response.body);
      return Future.value(json.decode(response.body));
    }else if(response.statusCode == 400 || response.statusCode == 422){
      return Future.error(json.decode(response.body));
    }else{
      return Future.error("Server error occurred");
    }
  }catch(e){
    print("----");
    print(e);
    print("----");
    return Future.error("Connection error, please try again.");
  }
}