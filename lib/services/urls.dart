class Urls {
  static final String base = "https://campussearch.in";//phourxx_-6e7340b4.localhost.run
  static final String mediaBase = "https://campussearch.in/storage/app/public";//phourxx_-6e7340b4.localhost.run
  static final String apiBase = "$base/api";
  static final String login = "$apiBase/login";
  static final String oAuthLogin = "$apiBase/oAuthLogin";
  static final String register = "$apiBase/register";
  static final String verifyOtp = "$apiBase/verifyOtp";
  static final String sendOtp = "$apiBase/sendOtp";
  static final String profile = "$apiBase/profile";
  static final String resetPassword = "$apiBase/resetPassword";
  static final String changePassword = "$apiBase/changePassword";
  static final String streams = "$apiBase/streams";
  static final String courses = "$apiBase/courses";
  static final String dashboard = "$apiBase/dashboard";
  static final String notifications = "$apiBase/notifications";
  static final String cities = "$apiBase/cities";
  static final String colleges = "$apiBase/colleges";
  static final String trendingColleges = "$apiBase/colleges/trending";
  static final String bookmarkCollege = "$apiBase/colleges/bookmark";
  static final String unbookmarkCollege = "$apiBase/colleges/unbookmark";
}