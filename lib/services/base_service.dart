import 'package:com/models/account/user.dart';

class BaseService{

  dynamic postAuthHeaders(){
    return {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer ${this.getUserToken()}"
    };
  }

  dynamic getAuthHeaders(){
    return {
      "Accept": "application/json",
      "Authorization": "Bearer ${this.getUserToken()}"
    };
  }

  dynamic postDefaultHeaders(){
    return {
      "Accept": "application/json",
      "Content-Type": "application/json",
    };
  }

  dynamic getDefaultHeaders(){
    return {
      "Accept": "application/json",
    };
  }

  String getUserToken(){
    return User.get().token;
  }
}