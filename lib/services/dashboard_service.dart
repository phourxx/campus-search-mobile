import 'package:com/models/account/dashboard.dart';
import 'package:com/models/account/notification.dart';
import 'package:com/services/base_service.dart';
import 'package:com/services/base_http.dart' as http;
import 'package:com/services/urls.dart';

class DashboardService extends BaseService{

  Future<Dashboard> fetch(){
    var headers = this.getAuthHeaders();
    return http.get(Urls.dashboard, headers: headers).then((response){
      return Dashboard.fromJson(response);
    });
  }

  Future<List<Notification>> fetchNotifications(){
    var headers = this.getAuthHeaders();
    return http.get(Urls.notifications, headers: headers).then((response){
      return Notification.fromJsonList(response);
    });
  }


}