import 'package:com/models/account/user.dart';
import 'package:com/models/stream/city.dart';
import 'package:com/models/stream/college.dart';
import 'package:com/models/stream/stream_object.dart';
import 'package:com/services/base_service.dart';
import 'package:com/services/base_http.dart' as http;
import 'package:com/services/urls.dart';
import 'package:com/utils/utils.dart';

class CollegeService extends BaseService{

  Future<List<City>> fetchCities(){
    var headers = this.getDefaultHeaders();
    return http.get(Urls.cities, headers: headers).then((response){
      return City.fromJsonList(response);
    });
  }

  Future<List<College>> list({bool trending=false, String query}){
    var headers = this.getAuthHeaders();
    return http.get(trending ? Urls.trendingColleges: Urls.colleges, headers: headers).then((response){
      return College.fromJsonList(response);
    });
  }

  Future<List<Course>> courses({String query}){
    var headers = this.getDefaultHeaders();
    return http.get(Urls.courses, headers: headers).then((response){
      return Course.fromJsonList(response);
    });
  }

  Future<List<College>> cityColleges({City city}){
    var headers = this.getAuthHeaders();
    return http.get("${Urls.cities}/${city.id}", headers: headers).then((response){
      return College.fromJsonList(response);
    });
  }

  Future<List<College>> streamColleges({StreamObject stream, City city}){
    var headers = this.getAuthHeaders();
    return http.get("${Urls.streams}/${stream.id}?city=${city.id}", headers: headers).then((response){
      return College.fromJsonList(response);
    });
  }

  Future<College> get(College college){
    var headers = this.getAuthHeaders();
    return http.get("${Urls.colleges}/${college.id}", headers: headers).then((response){
      return College.fromJson(response);
    });
  }

  Future<dynamic> bookmark([College college]){
    if(college == null){
      var headers = this.getAuthHeaders();
      return http.get("${Urls.bookmarkCollege}/", headers: headers).then((response){
        return College.fromJson(response);
      });
    }
    var headers = this.getAuthHeaders();
    return http.get("${Urls.bookmarkCollege}/${college.id}", headers: headers).then((response){
      return response;
    });
  }

  Future<dynamic> unbookmark([College college]){
    var headers = this.getAuthHeaders();
    return http.get("${Urls.unbookmarkCollege}/${college.id}", headers: headers).then((response){
      return response;
    });
  }

  Future<dynamic> postCollege(College college){
    User user = User.get();
    var headers;
    if(isNotEmpty(user?.token)){
      headers = this.postAuthHeaders();
    }else{
      headers = this.postDefaultHeaders();
    }
    return http.post(Urls.colleges, data: college.toJson(), headers: headers);
  }
}