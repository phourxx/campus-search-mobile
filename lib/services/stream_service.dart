import 'package:com/models/stream/stream_object.dart';
import 'package:com/services/base_service.dart';
import 'package:com/services/base_http.dart' as http;
import 'package:com/services/urls.dart';

class StreamService extends BaseService{
  Future<List<StreamObject>> fetchStreams(FetchStreamFilter filter){
    var headers = filter == FetchStreamFilter.selected
        ? this.getAuthHeaders() : this.getDefaultHeaders();
    return http.get(Urls.streams, headers: headers).then((response){
      return StreamObject.fromJsonList(response);
    });
  }

  Future<dynamic> saveStreams(List<StreamObject> streams){
    var headers = this.postAuthHeaders();
    return http.post(Urls.streams, data: {"streams": streams.map((e) => e.id).toList()}, headers: headers).then((response){
      return response;
    });
  }

  Future<StreamObject> viewStream(StreamObject streamObject){
    var headers = this.postAuthHeaders();
    return http.post("${Urls.streams}/${streamObject.id}", headers: headers).then((response){
      return StreamObject.fromJson(response);
    });
  }
}

enum FetchStreamFilter{
  all,
  selected,
}