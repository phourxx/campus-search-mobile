import 'package:com/models/account/user.dart';
import 'package:com/services/base_service.dart';
import 'package:com/services/base_http.dart' as http;
import 'package:com/services/urls.dart';

class AuthService extends BaseService{
  Future<dynamic> login(dynamic data){
    var headers = this.postDefaultHeaders();
    return http.post("${Urls.login}", data: data, headers: headers).then((response){
      return User.fromJson(response);
    });
  }
  Future<dynamic> oAuthLogin(dynamic data){
    var headers = this.postDefaultHeaders();
    return http.post("${Urls.oAuthLogin}", data: data, headers: headers).then((response){
      return User.fromJson(response);
    });
  }
  Future<dynamic> register(dynamic data){
    var headers = this.postDefaultHeaders();
    return http.post("${Urls.register}", data: data, headers: headers).then((response){
      return User.fromJson(response);
    });
  }
  Future<dynamic> verifyOtp(dynamic data){
    var headers = this.postDefaultHeaders();
    return http.post("${Urls.verifyOtp}", data: data, headers: headers).then((response){
      print("Respons");
      print(response);
      return response;
    });
  }
  Future<dynamic> changePassword(dynamic data){
    var headers = this.postAuthHeaders();
    return http.post("${Urls.changePassword}", data: data, headers: headers).then((response){
      print("Respons");
      print(response);
      return response;
    });
  }
  Future<dynamic> resetPassword(dynamic data){
    var headers = this.postDefaultHeaders();
    return http.post("${Urls.resetPassword}", data: data, headers: headers);
  }

  Future<dynamic> sendOtp(dynamic data){
    var headers = this.postDefaultHeaders();
    return http.post("${Urls.sendOtp}", data: data, headers: headers);
  }

  Future<dynamic> updateProfile(dynamic data){
    var headers = this.postAuthHeaders();
    return http.post("${Urls.profile}", data: data, headers: headers);
  }
}